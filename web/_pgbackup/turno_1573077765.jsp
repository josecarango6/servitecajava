<%-- 
    Document   : registroCliente
    Created on : 8/09/2019, 07:11:12 PM
    Author     : jooss
--%> 
<%@page import="modelo.DAO.TurnoDAO"%> 
<%@page import="static Controlador.servicioOpera.*"%> 
<%@page import="modelo.DAO.DescuentoDAO"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="modelo.DAO.TipoDocumentoDAO"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="Entidades.TipoDocumento"%> 
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@page import=" static Controlador.consultaCliente.*"%> 
<!doctype html> 
<html style="background: linear-gradient(90deg, #4b6cb7, #182848);"> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
    %> 
    <head> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>         
        <title>Registro Cliente</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <link href="bootstrap/css/searchbox.css" rel="stylesheet" type="text/css"/> 
    </head>     
    <body id="cuerpito"> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Turno</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Turnos Activos</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <!--cuadro de busqueda-->                                 
                                <div class='search-container'> 
</div>                                 
                                <!--cuadro de busqueda-->                                 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <script>
                        var nturno ;
                        var nplaca ;

                        $(document).ready(function () {

                            var limite = getBody($('table.Tablefu'));

                            $('#table-list tr').on('mouseover', function () {

                                for (var i = 0; i < limite; i++) {

                                    if (i === 0) {
                                        document.getElementById("seleccionado").value = $(this).find("td:eq(" + i + ")").html();
                                        dato2 = document.getElementById("seleccionado").value;
                                        nturno=dato2;

                                    }
                                    if (i === 1) {
                                        document.getElementById("seleccionado").value = $(this).find("td:eq(" + i + ")").html();
                                        dato2 = document.getElementById("seleccionado").value;
                                        nplaca=dato2;

                                    }

                                }
                            });

                        });


                        function getBody(element) {

                            var originalTable = element.clone();
                            var tds = $(originalTable).children('tbody').children('tr').length;
                            return tds;
                        }




                    </script>                     
                    <input style="display: none" id="seleccionado"> 
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left pr-5 pb-5 pl-5 col-md-12"> 
                            <h4 class="mb-3 mt-auto">Lista de Turnos</h4> 
                            <table class="table table-light Tablefu table-borded" id="table-list"> 
                                <thead class="table-dark"> 
                                    <tr> 
                                        <th>Numero de Turno</th> 
                                        <th>Placa del Vehiculo</th> 
                                        <th>Servicios</th> 
                                        <th>Pagar</th> 
                                    </tr>                                     
                                </thead>                                 
                                <tbody class="border-dark bg-dark"> 
                                    <%                                        TurnoDAO turno = new TurnoDAO();
                                        List<String> lista = new LinkedList();
                                        lista = turno.obtenerTurnosactivos();

                                        for (int i = 0; i < lista.size(); i++) {%> 
                                        <tr> 
                                            <td scope="row"><%out.print(lista.get(i));%></td> 
                                            <td><%out.print(lista.get(i+1));%></td> 
                                            <td><%out.print(lista.get(i+2));%></td> 
                                            <td>  <a href="liquidacion.jsp" onclick="this.href += '?turno=' + nturno + '&placa=' + nplaca + ''" class="btn btn-primary">Pagar</a>  </td> 
                                        </tr>                                         
                                        <%i = i + 2;%> 
                                    <%}%> 
                                </tbody>                                 
                            </table>                             
                            <!--<form class="needs-validation" novalidate="" method="post" action="registroCliente"> -->                             
                            <hr class="mb-4"> 
                            <!-- </form>   -->                             
                        </div>                         
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
    ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
                                                var ctx = document.getElementById("myChart");
                                                var myChart = new Chart(ctx, {
                                                    type: 'line',
                                                    data: {
                                                        labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                        datasets: [{
                                                                data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                                lineTension: 0,
                                                                backgroundColor: 'transparent',
                                                                borderColor: '#007bff',
                                                                borderWidth: 4,
                                                                pointBackgroundColor: '#007bff'
                                                            }]
                                                    },
                                                    options: {
                                                        scales: {
                                                            yAxes: [{
                                                                    ticks: {
                                                                        beginAtZero: false
                                                                    }
                                                                }]
                                                        },
                                                        legend: {
                                                            display: false,
                                                        }
                                                    }
                                                }
                                                );
        </script>         
    </body>     
</html>
