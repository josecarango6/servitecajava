package Entidades;

import Entidades.ServiciosPrestados;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-09T17:04:40")
@StaticMetamodel(Servicio.class)
public class Servicio_ { 

    public static volatile SingularAttribute<Servicio, String> nombreServicio;
    public static volatile SingularAttribute<Servicio, Integer> idSERVICIO;
    public static volatile SingularAttribute<Servicio, Integer> costo;
    public static volatile ListAttribute<Servicio, ServiciosPrestados> serviciosPrestadosList;

}