<%-- 
    Document   : cosultaCliente
    Created on : 15/09/2019, 10:53:38 AM
    Author     : jooss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.DAO.ClienteDAO"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Cover Template for Bootstrap</title>
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="cover.css" rel="stylesheet">
    </head>
    <body>


        <div class="site-wrapper">
            <div class="site-wrapper-inner bg-dark">
                <div class="cover-container">
                    <div class="masthead clearfix">
                    </div>

                    <%
                        int cedula = Integer.parseInt(request.getParameter("cedula"));
                        //metodo consulta imprime un mensaje si esta registrado o no
                        ClienteDAO cldao = new ClienteDAO();
                        int val = cldao.ConsultarnumeroDocumento(cedula);
                        if (val == 0) {%>
                    <h1>Cliente no se encuentra registrado </h1>

                    <% } else {%>
                    <h1>Cliente se encuentra registrado </h1>
                    <%}
                    %>
                    <div class="mastfoot">
                        <div class="inner">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
