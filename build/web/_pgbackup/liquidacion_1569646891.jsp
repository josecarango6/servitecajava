<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="modelo.DAO.LiquidacionDAO"%>
<%@page import="modelo.DAO.EstadoLiquidacionDAO"%>
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%>
<%@page import="modelo.DAO.ServicioDAO"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<%@page import=" static Controlador.traedatos.*"%>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard Template for Bootstrap</title>
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="dashboard.css" rel="stylesheet">
        <!--clockpicker-->
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>
        <link rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css">
        <!--clockpicker-->
    </head>
    <body>
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
            <a class="navbar-brand col-sm-3 col-md-3 mr-0" href="#">Service Station System</a>
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>                 
                <li class="breadcrumb-item">
                    <a href="#">Library</a>
                </li>                 
                <li class="breadcrumb-item active">Data</li>                 
            </ol>
            <ul class="navbar-nav px-3">
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="#">Sign out</a>
                </li>
            </ul>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="index.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"> 
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>                                         
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>                                         
                                    </svg>                                Inicio <span class="sr-only">(current)</span> </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="liquidacion.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file">
                                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                                        <polyline points="13 2 13 9 20 9"></polyline>
                                    </svg>
                                    Liquidacion </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="registroVehiculo.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 11 11">
                                        <path d="M9 4l-.89-2.66A.5.5 0 0 0 7.64 1H3.36a.5.5 0 0 0-.47.34L2 4a1 1 0 0 0-1 1v3h1v1a1 1 0 1 0 2 0V8h3v1a1 1 0 1 0 2 0V8h1V5a1 1 0 0 0-1-1zM3 7a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0-3l.62-2h3.76L8 4H3zm5 3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" fill="#626262"></path>
                                    </svg>
                                    Registro Vehiculo </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                    Registro Cliente </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 20 20">
                                        <path d="M4 5H.78c-.37 0-.74.32-.69.84l1.56 9.99S3.5 8.47 3.86 6.7c.11-.53.61-.7.98-.7H10s-.7-2.08-.77-2.31C9.11 3.25 8.89 3 8.45 3H5.14c-.36 0-.7.23-.8.64C4.25 4.04 4 5 4 5zm4.88 0h-4s.42-1 .87-1h2.13c.48 0 1 1 1 1zM2.67 16.25c-.31.47-.76.75-1.26.75h15.73c.54 0 .92-.31 1.03-.83.44-2.19 1.68-8.44 1.68-8.44.07-.5-.3-.73-.62-.73H16V5.53c0-.16-.26-.53-.66-.53h-3.76c-.52 0-.87.58-.87.58L10 7H5.59c-.32 0-.63.19-.69.5 0 0-1.59 6.7-1.72 7.33-.07.37-.22.99-.51 1.42zM15.38 7H11s.58-1 1.13-1h2.29c.71 0 .96 1 .96 1z" fill="#626262"></path>
                                    </svg>
                                    Servicios </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 1024 1024">
                                        <path d="M855.7 210.8l-42.4-42.4a8.03 8.03 0 0 0-11.3 0L168.3 801.9a8.03 8.03 0 0 0 0 11.3l42.4 42.4c3.1 3.1 8.2 3.1 11.3 0L855.6 222c3.2-3 3.2-8.1.1-11.2zM304 448c79.4 0 144-64.6 144-144s-64.6-144-144-144-144 64.6-144 144 64.6 144 144 144zm0-216c39.7 0 72 32.3 72 72s-32.3 72-72 72-72-32.3-72-72 32.3-72 72-72zm416 344c-79.4 0-144 64.6-144 144s64.6 144 144 144 144-64.6 144-144-64.6-144-144-144zm0 216c-39.7 0-72-32.3-72-72s32.3-72 72-72 72 32.3 72 72-32.3 72-72 72z" fill="#626262"></path>
                                    </svg>
                                    Descuentos</a>
                            </li>
                        </ul>
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted"> <span>Reportes</span> <a class="d-flex align-items-center text-muted" href="#"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="16"></line>
                                    <line x1="8" y1="12" x2="16" y2="12"></line>
                                </svg> </a> </h6>
                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                        <polyline points="14 2 14 8 20 8"></polyline>
                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                        <polyline points="10 9 9 9 8 9"></polyline>
                                    </svg>
                                    Reporte del mes </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                        <polyline points="14 2 14 8 20 8"></polyline>
                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                        <polyline points="10 9 9 9 8 9"></polyline>
                                    </svg>
                                    Last quarter </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                        <polyline points="14 2 14 8 20 8"></polyline>
                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                        <polyline points="10 9 9 9 8 9"></polyline>
                                    </svg>
                                    Social engagement </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text">
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                        <polyline points="14 2 14 8 20 8"></polyline>
                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                        <polyline points="10 9 9 9 8 9"></polyline>
                                    </svg>
                                    Year-end sale </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mb-5">
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 class="h2">Liquidacion de Servicios</h1>
                        <div class="btn-toolbar mb-2 mb-md-0">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-secondary">Share</button>
                                <button class="btn btn-sm btn-outline-secondary">Export</button>
                            </div>
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar">
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                    <line x1="16" y1="2" x2="16" y2="6"></line>
                                    <line x1="8" y1="2" x2="8" y2="6"></line>
                                    <line x1="3" y1="10" x2="21" y2="10"></line>
                                </svg>
                                This week
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="order-md-1 text-left bg-white pr-5 pb-5 pl-5 col-md-12">
                            <h4 class="mb-3 mt-auto">Datos del Vehiculo</h4>
                            <form class="needs-validation" novalidate="" action="registroLiquidacion" method="post">
                                <div class="row">
                                    <div class="mb-3 col-md-6 col-lg-3">Fecha de Ingreso
                                        <br>
                                        <input type="date" class="form-control" id="fecha" value="<%if (dateFecha != null) {
                                                out.print(dateFecha);
                                            }%>">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <div class="mb-3 col-md-3">Placa del Vehiculo
                                        <br>
                                        <input type="text" class="form-control" id="placa" placeholder="" value="<%if (val3 != null) {
                                                out.print(val3);
                                            }%>" required="">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <div class="mb-3 col-md-3">Turno
                                        <br>
                                        <input type="text" class="form-control" id="turno" placeholder="" value="<%if (turno != 0) {
                                                out.print(turno);
                                            }%>" required="">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <!--BOTON DE CONSULTA-->
                                    <input class="btn btn-primary btn-lg btn-block mb-5 m-auto col-md-3" onclick="window.frames['ventana_iframe'].document.getElementById('automatico').click();
                                            location.reload();" type="button" value="consultar">
                                    <iframe style="display: none" d='frm1' src='traedatos' marginwidth='0' marginheight='0' name='ventana_iframe' scrolling='no' border='0' frameborder='0' width='300' height='200'>
</iframe>
                                    <!--este consulta-->
                                    <div class="invalid-feedback">
                                        Valid last name is required.
</div>
                                </div>
                                <hr class="bg-primary">
                                <div class="row p-auto mr-auto ml-auto mb-2 mt-2">
                                    <h4 class="m-5 m-auto col-md-4">Servicios Realizados</h4>
                                    <h4 class="m-auto col-md-4">Costo del Servicio</h4>
                                    <h4 class="m-auto col-md-4">Costo Total Servicioso</h4>
                                </div>
                                <div class="row">
                                    <div class="btn-group-vertical col-md-4 " data-toggle="buttons"> 
                                        <%//trae del servlet traedatos

                                            //prueba
                                            List<Integer> lstPRESTADOS = new LinkedList();
                                            List<String> lstPRESTADOS2 = new LinkedList();

                                            ServiciosPrestadosDAO buscaid = new ServiciosPrestadosDAO();
                                            ServiciosPrestadosDAO busca = new ServiciosPrestadosDAO();
                                            //tipo vehiculo

                                            String tipoveh = "";
                                            LiquidacionDAO ConsultaID = new LiquidacionDAO();
                                            int idvehiculo = ConsultaID.consultaIdVehiculo(turno, dateFecha, val3);

                                            //id vehiculo
                                            tipoveh = busca.conTipoVehiculoSelect(turno, dateFecha, val3);
                                            lstPRESTADOS = buscaid.conServiSelect(turno, dateFecha, val3);
                                            //setiar
                                            turno = 0;

                                            val3 = "";

                                            String captura = "";

                                            for (int i = 0; i < lstPRESTADOS.size(); i++) {

                                                captura = busca.obtenerNombreServicio(lstPRESTADOS.get(i));
                                                lstPRESTADOS2.add(captura);

                                            }
                                            java.util.Collections.sort(lstPRESTADOS2);//ordena alfabeticamente
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {%>
                                            <label class="btn btn-secondary" for="<%out.print(lstPRESTADOS2.get(i));%>">
                                                <input type="checkbox" checked="" disabled="disabled" value="<%out.print(lstPRESTADOS2.get(i));%>" autocomplete="off" d="<%out.print(lstPRESTADOS2.get(i));%>" name="servicio">
                                                <%out.print(lstPRESTADOS2.get(i));
                                            %>
                                            </label>
                                        <%}%>
                                    </div>
                                    <div class="col-md-6 col-lg-4"> 
                                        <%

                                            List<String> listacostos = new LinkedList();
                                            List<Integer> listacostosformato = new LinkedList();
                                            String captura2 = "";
                                            int capturanumFormato = 0;
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {
                                                captura2 = busca.selectcostostr(tipoveh, lstPRESTADOS2.get(i));
                                                listacostos.add(captura2);
                                                capturanumFormato = busca.selectcostoFrmat(tipoveh, lstPRESTADOS2.get(i));
                                                listacostosformato.add(capturanumFormato);
                                            }%>
                                        <% NumberFormat nf = NumberFormat.getInstance();// le da formato al numero
                                            nf = NumberFormat.getIntegerInstance();

                                            int sumacostos = 0;
                                            int contarServicios = 0;
                                            for (int i = 0; i < listacostos.size(); i++) {%>
                                            <button class="form-control mb-2" id="<%out.print(listacostos.get(i));%>" disabled="disabled">
                                                <%out.print(nf.format(listacostosformato.get(i)));%>
                                                <%sumacostos = sumacostos + Integer.parseInt(listacostos.get(i));%>
                                                <%contarServicios = contarServicios + 1;%>
                                            </button>
                                        <%}

                                        %>
                                        <div class="invalid-feedback">
</div>
                                    </div>
                                    <div class="col-md-4 mb-0">
                                        <input type="text" class="form-control mb-5" id="costoServicios" name="costoServicios" placeholder="" value="<%out.print(sumacostos);%>" required="" readonly="readonly">
                                        <div class="invalid-feedback">
                                            Valid last name is required.
</div>                                         
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Valid first name is required.
</div>
                                <div class="row p-auto mt-3">
                                    <div class="col-md-6 mb-3 col-lg-4"> 
                                        <h4 class="p-auto ml-auto mb-auto mr-auto mt-auto ml-3 mb-3 mr-3 m-auto">Descuento %</h4>
                                    </div>
                                    <div class="col-md-6 col-lg-4 mt-auto mb-auto">Porcetaje de descuento
                                        <br>
                                        <%LiquidacionDAO liqui = new LiquidacionDAO();
                                            int cant = liqui.cantidadServicio();
                                            int porc = 0;
                                            int totalLiquidacion = 0;
                                            if (contarServicios >= cant) {
                                                porc = liqui.PorcentajeDesc();
                                                totalLiquidacion = (sumacostos * porc) / 100;
                                                totalLiquidacion = sumacostos - totalLiquidacion;
                                            }
                                            if (contarServicios < cant) {
                                                totalLiquidacion = sumacostos;
                                            }

                                        %>
                                        <input type="text" class="form-control" id="porcentaje" name="porcentaje" placeholder="" value="<%out.print(porc);%>" required="" readonly="readonly">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <div class="mb-3 col-md-4">
                                        <h4 class="m-auto">Valor Total a Pagar</h4>
                                        <input type="text" class="form-control" id="totalLiquidacion" name="totalLiquidacion" placeholder="" value="<%out.print(totalLiquidacion);%>" required="" readonly="readonly">
                                        <div class="invalid-feedback">
                                            Valid last name is required.
</div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="mb-3 col-md-4"></div>
                                    <div class="mb-3 col-md-4"></div>
                                    <div class="mb-3 col-md-4">
                                        <h4 class="m-auto">Valor Total a Pagar</h4>
                                        <input type="text" class="form-control" id="totalLiquidacion" name="totalLiquidacion" placeholder="" value="<%out.print(totalLiquidacion);%>" required="" readonly="readonly">
                                        <div class="invalid-feedback">
                                            Valid last name is required.
</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="display: none" lass="col-md-6 mb-3">FechaIngreso
                                        <br>
                                        <input type="date" class="form-control" id="FechaIngreso" name="FechaIngreso" placeholder="" value="<%out.print(dateFecha);%>" required="">
                                        <%//setiando
                                            dateFecha = null;%>
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <div style="display: none" lass="col-md-6 mb-3">idvehiculo
                                        <br>
                                        <input type="text" class="form-control" id="idVehiculo" placeholder="" name="idVehiculo" value="<%out.print(idvehiculo);%>" required="">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <div class="col-md-6 mb-3">Fecha de Salida
                                        <br>
                                        <input type="date" class="form-control" id="fechaSalida" name="fechaSalida" placeholder="" value="" required="">
                                        <div class="invalid-feedback">
                                            Valid first name is required.
</div>
                                    </div>
                                    <div class="col-md-6 mb-3">Hora de Salida
                                        <br>
                                        <input type="text" class="form-control" id="horaSalida" name="horaSalida" placeholder="" value="" required="" readonly="readonly">
                                        <script>
                                            $("input[name=horaSalida]").clockpicker({
                                                placement: 'bottom',
                                                align: 'left',
                                                autoclose: true,
                                                default: 'now',
                                                donetext: "Select"
                                            });
                                        </script>
                                        <div class="invalid-feedback">
                                            Valid last name is required.
</div>
                                    </div>                                     
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">Estado de la Liquidacion
                                        <br> 
                                        <% List<String> listaEstados = new LinkedList();
                                            EstadoLiquidacionDAO estado = new EstadoLiquidacionDAO();
                                            listaEstados = estado.obtenerTodosLosEstadosLiquidacion();%> 
                                        <select class="custom-select d-block w-100" required="" id="estadoLiquidacion" name="estadoLiquidacion"> 
                                            <option value="" disabled selected="disabled selected">seleccione una opcion</option>                                             
                                            <%
                                                //LLenandose :)
                                                for (int i = 0; i < listaEstados.size(); i++) {%> 
                                                <option name="tipos">
                                                    <%out.print(listaEstados.get(i));%>
                                                </option>                                                 
                                            <%  }%> 
                                        </select>                                         
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
</div>                                         
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Valid first name is required.
</div>
                                <hr class="mb-4 bg-primary">
                                <div class="row">
                                    <button class="btn btn-primary btn-lg btn-block mb-5 col-md-5 m-auto" href="Inicio.html" type="submit">Atras</button>
                                    <button class="btn btn-primary btn-lg btn-block col-md-5 m-auto" type="submit">Imprimir</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- Icons -->
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
        <script>feather.replace()</script>
        <!-- Graphs -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
        <script>
                                            var ctx = document.getElementById("myChart");
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                data: {
                                                    labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                    datasets: [{
                                                            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                            lineTension: 0,
                                                            backgroundColor: 'transparent',
                                                            borderColor: '#007bff',
                                                            borderWidth: 4,
                                                            pointBackgroundColor: '#007bff'
                                                        }]
                                                },
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: false
                                                                }
                                                            }]
                                                    },
                                                    legend: {
                                                        display: false,
                                                    }
                                                }
                                            });
        </script>
    </body>
</html>