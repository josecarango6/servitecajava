<%-- 
    Document   : index
    Created on : 8/09/2019, 4:11:44 p. m.
    Author     : jooss
--%> 
<%@page import="java.text.DateFormat"%> 
<%@page import="java.text.SimpleDateFormat"%> 
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%> 
<%@page import="modelo.DAO.ReportesDAO"%> 
<%@page import="java.sql.Date"%> 
<%@page import="static Controlador.ReporteServicio.*"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="java.util.List"%> 
<%@page import="java.util.LinkedList"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!doctype html> 
<html lang="en"> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Dashboard Template for Bootstrap</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
    </head>     
    <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Cantidad de Servicios Prestados y Tiempo de Atención</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <%            String[] fechas = new String[0];
            List<String> listafecha = new LinkedList();
            String lista = "";
            int[] idServicios = new int[0];
            if (Servicios != null) {
                idServicios = new int[Servicios.length];
            }

            ReportesDAO rserv = new ReportesDAO();
            try {

                Date dateFecha1 = new Date(System.currentTimeMillis());
                Date dateFecha2 = new Date(System.currentTimeMillis());

                long dias = 0;
                int y = 0;

                if (fecha1 != null && fecha2 != null) {
                    dateFecha1 = Date.valueOf(fecha1);
                    dateFecha2 = Date.valueOf(fecha2);
                    long diferenciaMils = dateFecha2.getTime() - dateFecha1.getTime();

                    //obtenemos los segundos
                    long segundos = diferenciaMils / 1000;

                    //obtenemos las horas
                    long horas = segundos / 3600;
                    dias = horas / 24;//rengo de fechas
                    y = Math.toIntExact(dias) + 1;
                    fechas = new String[y];

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String text = df.format(dateFecha1);
                    for (int i = 0; i < y; i++) {

                        fechas[i] = rserv.sumarDiasFecha2(text, i);

                    }

                }

                ServiciosPrestadosDAO daoServiPresta = new ServiciosPrestadosDAO();

                for (int j = 0; j < Servicios.length; j++) {

                    for (int i = 0; i < y; i++) {

                        idServicios[j] = daoServiPresta.obtenerIdServicio(Servicios[j]);

                    }
                }
            } catch (NullPointerException e) {

            }
        %> 
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky text-justify"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Cantidad de Servicios Prestados y Tiempo Promedio</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                                    <div class="order-md-1 text-left pr-5 pb-5 pl-5 col-md-12"> 
                        <h4 class="mb-3 mt-auto">Reporte de Servicios Prestados</h4> 
                        <form class="needs-validation" novalidate="" method="post" action="ReporteServicio"> 
                            <div class="row"> 
                                <div class="col-md-6 mb-3 mt-2 col-lg-3">Seleccione el Tipo de Reporte
                                    <br> 
                                    <script>

                                        function tiposelect() {
                                            var option = document.getElementById("tipoReporte");
                                            var checks = document.getElementById('buttons');


                                            if (option.value === "1") {

                                                checks.style.display = 'block';
                                            } else {
                                                checks.style.display = 'none';
                                            }


                                        }

                                    </script>                                     
                                    <select id="tipoReporte" onchange="tiposelect()" class="custom-select" name="tiporeporte"> 
                                        <option selected>Seleccione una opción</option>                                         
                                        <option value="1">Cantidad de Servicios Prestados</option>                                         
                                        <option value="2">Tiempo Promedio de Atención</option>                                         
                                    </select>                                     
                                    <div class="invalid-feedback"> 
                                        Valid first name is required.
</div>                                     
                                </div>                                 
                                <div class="col-md-6 mb-3 mt-2 col-lg-3">Fecha de Consulta Desde
                                    <br> 
                                    <input type="date" class="form-control" id="fecha" placeholder="ejemplo@ejemplo.com" name="fechainicio"> 
                                    <div class="invalid-feedback"> 
                                        Valid first name is required.
</div>                                     
                                </div>                                 
                                <div class="col-md-6 mb-3 mt-2 col-lg-3">Fecha de Consulta Hasta
                                    <br> 
                                    <input type="date" class="form-control" id="fecha" placeholder="ejemplo@ejemplo.com" name="fechafin"> 
                                    <div class="invalid-feedback"> 
                                        Valid first name is required.
</div>                                     
                                </div>                                 
                                <div id="buttons" data-toggle="buttons" class="btn-group-vertical col-lg-3" style="display: none"> Selecione los Servicios
                                    <%                                            //prueba
                                        List<String> lstDoc = new LinkedList();
                                        ServicioDAO tiposervicio = new ServicioDAO();
                                        lstDoc = tiposervicio.obtenerTodosLosServicios();//LLenandose :)
                                        for (int i = 0; i < lstDoc.size(); i++) {%> 
                                        <label class="btn btn-secondary" for="<%out.print(lstDoc.get(i));%>"> 
                                            <input type="checkbox" value="<%out.print(lstDoc.get(i));%>" autocomplete="off" name="servicio"> 
                                            <%out.print(lstDoc.get(i));
                                        %> 
                                        </label>                                         
                                    <%}%> 
                                </div>                                 
                            </div>                             
                            <div class="row"> 
</div>                             
                            <hr class="mb-4"> 
                            <div class="row mb-5"> 
                                <button class="btn btn-primary btn-lg btn-block m-auto col-md-4" type="submit">Consultar</button>                                 
                            </div>                             
                        </form>                         
                        <table style="width: 100px;float: left" class="table table-light table-bordered"> 
                            <thead class="table-dark"> 
                                <tr> 
                                    <th style="height: 80px">Fechas</th> 
                                </tr>                                 
                            </thead>                             
                                <tbody> 
                                <%for (int i = 0; i < fechas.length; i++) {%> 
                                <tr> 
                                        <td><%out.print(fechas[i]);%></td> 
                                    <% }
                                    %> 
                                    </tr>                                     
                            </tbody>                             
                        </table>                         
                        <%if (tiporeporte != null) {%> 
                            <%if (tiporeporte.equals("1")) {%> 
                                <%int pos = 0;%> 
                                <%if (Servicios != null) {%> 
                                    <%for (int w = 0; w < Servicios.length; w++) {%> 
                                        <table style="width: 100px;float: left" class="table"> 
                                            <thead> 
                                                    <tr> 
                                                    <%--<th class="fechasocultas" width="10%">Fecha</th>--%> 
                                                    <%if (Servicios != null) {%> 
                                                    <th style="height: 80px"><%out.print(Servicios[w]);
                                        }%></th> 
                                                </tr>                                                 
                                            </thead>                                             
                                            <tbody> 
                                                <%for (int i = 0; i < fechas.length; i++) {%> 
                                                    <tr> 
                                                        <%--<td class="fechasocultas" width="10%"><%out.print(fechas[i]);%> 
                                                    </td>                                                                                                                                                                                                                --%>
                                                    <%  lista = rserv.ReporteCantidadServicios(idServicios[w], fechas[i]);

                                        listafecha.add(lista);%> 
                                                    <%for (int j = pos; j < listafecha.size(); j++) {%> 
                                                        <td width="5%"><%out.print(listafecha.get(i + j));%></td> 
                                                        <%if (i == fechas.length - 1) {
                                            pos = listafecha.size() - 1 / Servicios.length;
                                        }%> 
                                                    <%break;
                                        }%> 
                                                <%}%> 
                                            </tr>                                             
                                        </tbody>                                         
                    </table>                     
                <%}
                            }%> 
            <%}
                            }%> 
            <%if (tiporeporte != null) {%> 
                <%if (tiporeporte.equals("2")) {%>                      
                    <table style="width: 100px;float: left" class="table table-light table-bordered"> 
                        <thead class="table-dark"> 
                            <th style="height: 80px">Dias</th> 
                        </thead>                         
                            <tbody> 
                            <%Double[] resultado = new Double[fechas.length];

                                    for (int i = 0; i < fechas.length; i++) {%> 
                            <tr> 
                                    <%int cant = rserv.cantidadTurnos(fechas[i]);
                                        resultado[i] = (double) rserv.Reportedias(fechas[i]) / cant;

                                    %> 
                                    <td><%out.print(resultado[i]);%></td> 
                                <%}%> 
                                </tr>                                 
                        </tbody>                         
                    </table>                     
                <%}%> 
            <%}%> 
            </div>             
        </main>         
    </div>     
</div> 
<!-- Bootstrap core JavaScript
            ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/popper.js"></script> 
<script src="bootstrap/js/bootstrap.min.js"></script> 
<!-- Icons --> 
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script> 
<script>feather.replace()</script> 
<!-- Graphs --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script> 
<script>
                                        var ctx = document.getElementById("myChart");
                                        var myChart = new Chart(ctx, {
                                            type: 'line',
                                            data: {
                                                labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                datasets: [{
                                                        data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                        lineTension: 0,
                                                        backgroundColor: 'transparent',
                                                        borderColor: '#007bff',
                                                        borderWidth: 4,
                                                        pointBackgroundColor: '#007bff'
                                                    }]
                                            },
                                            options: {
                                                scales: {
                                                    yAxes: [{
                                                            ticks: {
                                                                beginAtZero: false
                                                            }
                                                        }]
                                                },
                                                legend: {
                                                    display: false,
                                                }
                                            }
                                        });
        </script>
