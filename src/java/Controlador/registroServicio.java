package Controlador;

import Entidades.Servicio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.ServicioDAO;

@WebServlet(name = "registroServicio", urlPatterns = {"/registroServicio"})
public class registroServicio extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            String nombreServicio = request.getParameter("nombreServicio");
            int costoAutomovil = Integer.parseInt(request.getParameter("costoAutomovil"));
            int costoCampero = Integer.parseInt(request.getParameter("costoCampero"));
            int costoCamion = Integer.parseInt(request.getParameter("costoCamion"));
            int costoBus = Integer.parseInt(request.getParameter("costoBus"));
            ServicioDAO serv = new ServicioDAO();
            Servicio servicio = new Servicio(nombreServicio, costoAutomovil, costoCampero, costoCamion, costoBus);
            serv.guardar(servicio);
            RequestDispatcher vista = request.getRequestDispatcher("/servicios.jsp");
            vista.forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(registroServicio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
