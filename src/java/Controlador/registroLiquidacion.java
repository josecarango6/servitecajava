package Controlador;

import Entidades.Liquidacion;
import java.io.IOException;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.EstadoLiquidacionDAO;
import modelo.DAO.LiquidacionDAO;
import modelo.DAO.TurnoDAO;

@WebServlet(name = "registroLiquidacion", urlPatterns = {"/registroLiquidacion"})
public class registroLiquidacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            LiquidacionDAO liquida = new LiquidacionDAO();
            int costoServicios = Integer.parseInt(request.getParameter("costoServicios"));
            int porcentaje = Integer.parseInt(request.getParameter("porcentaje"));
            int totalLiquidacion = Integer.parseInt(request.getParameter("totalLiquidacion"));

            int idVehiculo = Integer.parseInt(request.getParameter("idVehiculo"));

            String fechaSalida = request.getParameter("fechaSalida");
            Date dateFechaingreso = liquida.consultarfecha(idVehiculo);
            String horaIngreso = liquida.consultarhora(idVehiculo);
            Date dateFechaSalida = Date.valueOf(fechaSalida);
            String horaSalida = request.getParameter("horaSalida") + ":00";
            String estadoLiquidacion = request.getParameter("estadoLiquidacion");
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            java.util.Date date1 = formatter.parse(horaIngreso);
            java.util.Date date2 = formatter.parse(horaSalida);

            EstadoLiquidacionDAO liquid = new EstadoLiquidacionDAO();
            int descuento = liquid.obtenerIdDescuento(porcentaje);
            int idEstado = liquid.obtenerIdestado(estadoLiquidacion);
            DateFormat tiemposervicio;
            DateFormat diaservicio;

            tiemposervicio = new SimpleDateFormat("HH:mm:ss");
            diaservicio = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date fecha1 =diaservicio.parse(diaservicio.format(dateFechaingreso));
            java.util.Date fecha2 = diaservicio.parse(diaservicio.format(dateFechaSalida));

            java.util.Date difference = getDifferenceBetwenDates(date1, date2);
            int diferenciadias= numeroDiasEntreDosFechas(fecha1, fecha2);
            String tiempo = tiemposervicio.format(difference);
            

            //Guardamos
            TurnoDAO turno= new TurnoDAO();
            LiquidacionDAO inserta = new LiquidacionDAO();
            if (idEstado == 1) {
                Liquidacion u = new Liquidacion(descuento, dateFechaingreso, horaIngreso, costoServicios, totalLiquidacion, dateFechaSalida, horaSalida, tiempo,diferenciadias, idVehiculo, idEstado);
                inserta.guardar(u);
                //UPDATE turno
                turno.modificarEstadoturno(idVehiculo);
            }
//si no es igual a 1 entonces imprima liquidacion para pagar con codigo de barrras

            RequestDispatcher vista = request.getRequestDispatcher("/liquidacion.jsp");
            vista.forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(registroLiquidacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static java.util.Date getDifferenceBetwenDates(java.util.Date dateInicio, java.util.Date dateFinal) {
        long milliseconds = dateFinal.getTime() - dateInicio.getTime();
        int seconds = (int) (milliseconds / 1000) % 60;
        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.SECOND, seconds);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.HOUR_OF_DAY, hours);
        return c.getTime();
    }
    public static int numeroDiasEntreDosFechas(java.util.Date fecha1, java.util.Date fecha2){
     long startTime = fecha1.getTime();
     long endTime = fecha2.getTime();
     long diffTime = endTime - startTime;
     return (int)TimeUnit.DAYS.convert(diffTime, TimeUnit.MILLISECONDS);
}

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
