package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jooss
 */
public class ReportesDAO {

    private Connection conexion;

    public ReportesDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public String ReporteCantidadServicios(int idservi, String fechaServicio) throws SQLException {

        String sql = "select count(FK_idSERVICIO) from servicios_prestados inner join "
                + " vehiculo on FK_idVEHICULO=idVEHICULO where FK_idSERVICIO = ? and fechaServicio =?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        Date date = new Date(0);
        if (fechaServicio != null) {
            date = Date.valueOf(fechaServicio);
        }

        ps1.setInt(1, idservi);
        ps1.setDate(2, date);
        // ps1.setDate(3, fechaServicio2);

        ResultSet rs = ps1.executeQuery();

        String servicio = "0";
        while (rs.next()) {

            servicio = rs.getString(1);

        }

        ps1.close();
        return servicio;
    }

    public String sumarDiasFecha2(String fecha, int dias) throws ParseException {

        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final java.util.Date date = df.parse(fecha); // conversion from String
        final java.util.Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(date);
        cal.add(GregorianCalendar.DAY_OF_YEAR, dias); // date manipulation
        return df.format(cal.getTime()); // conversion to String

    }

    public String Reportetiempo(String fechaServicio, int cant) throws SQLException {

        List<String> listaTipos = new LinkedList<>();
        String sql = " select tiempoServicio from liquidacion inner join servicios_prestados on "
                + " liquidacion.FK_idVEHICULO=servicios_prestados.FK_idVEHICULO where fechaIngreso= ? group by turno ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        Date date = new Date(0);
        if (fechaServicio != null) {
            date = Date.valueOf(fechaServicio);
        }

        ps1.setDate(1, date);
        // ps1.setDate(3, fechaServicio2);

        ResultSet rs = ps1.executeQuery();

        String tiempo = "0";
        while (rs.next()) {

            tiempo = rs.getString(1);
            listaTipos.add(tiempo);
        }
        int acumhoras = 0;
        for (int i = 0; i < listaTipos.size(); i++) {
            String[] parts = listaTipos.get(i).split(":");
            acumhoras = acumhoras + Integer.valueOf(parts[0]);
        }
        int acuminutos = 0;
        for (int i = 0; i < listaTipos.size(); i++) {
            String[] parts = listaTipos.get(i).split(":");
            acuminutos = acuminutos + Integer.valueOf(parts[1]);
        }

        String total = acumhoras / cant + ":" + acuminutos / cant;

        ps1.close();
        return total;
    }

    public Integer Reportedias(String fechaServicio) throws SQLException {
        List<Integer> lista = new LinkedList<>();
        String sql = "select diasServicio from liquidacion inner join servicios_prestados on "
                + " liquidacion.FK_idVEHICULO=servicios_prestados.FK_idVEHICULO where fechaIngreso= ? group by turno";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        Date date = new Date(0);
        if (fechaServicio != null) {
            date = Date.valueOf(fechaServicio);
        }

        ps1.setDate(1, date);
        // ps1.setDate(3, fechaServicio2);

        ResultSet rs = ps1.executeQuery();

        int servicio = 0;
        while (rs.next()) {

            servicio = rs.getInt(1);
            lista.add(servicio);
        }
        int suma = 0;
        for (int i = 0; i < lista.size(); i++) {
            suma = suma + lista.get(i);
        }

        ps1.close();
        return suma;
    }

    public Integer cantidadTurnos(String fechaServicio) throws SQLException {
        List<Integer> lista = new LinkedList<>();
        String sql = "select diasServicio from liquidacion inner join servicios_prestados on "
                + " liquidacion.FK_idVEHICULO=servicios_prestados.FK_idVEHICULO where fechaIngreso= ? group by turno";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        Date date = new Date(0);
        if (fechaServicio != null) {
            date = Date.valueOf(fechaServicio);
        }

        ps1.setDate(1, date);
        // ps1.setDate(3, fechaServicio2);

        ResultSet rs = ps1.executeQuery();

        int servicio = 0;
        while (rs.next()) {

            servicio = rs.getInt(1);
            lista.add(servicio);
        }
        if (lista.isEmpty()) {
            ps1.close();
            return 1;
        } else {

            ps1.close();
            return lista.size();
        }
    }

//    public String ReporteCantidadxplaca(String placa, String nombreServicio) throws SQLException {
//
//        String sql = "select  count(nombreServicio)from servicios_prestados inner join "
//                + "servicio on FK_idSERVICIO = idSERVICIO inner join vehiculo on FK_idVEHICULO = idVEHICULO  "
//                + "where placa = ? and nombreServicio=? group by fechaServicio ";
//
//        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
//        PreparedStatement ps1 = conexion.prepareStatement(sql);
//
//        ps1.setString(1, placa);
//
//        ps1.setString(2, nombreServicio);
//
//        ResultSet rs = ps1.executeQuery();
//
//        String servicio = "";
//        while (rs.next()) {
//
//            servicio = servicio + rs.getString(1) + ",";
//
//        }
//
//        ps1.close();
//        return servicio;
//    }
//
//    public List<String> Reportefechasxplaca(String placa) throws SQLException {
//        List<String> lista = new LinkedList<>();
//        String sql = "select fechaServicio from servicios_prestados inner join "
//                + "servicio on FK_idSERVICIO = idSERVICIO inner join vehiculo on FK_idVEHICULO = idVEHICULO  "
//                + "where placa = ?  group by fechaServicio  ";
//
//        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
//        PreparedStatement ps1 = conexion.prepareStatement(sql);
//
//        ps1.setString(1, placa);
//
//        ResultSet rs = ps1.executeQuery();
//
//        String servicio = "";
//        while (rs.next()) {
//
//            servicio = rs.getString(1);
//            lista.add(servicio);
//        }
//
//        ps1.close();
//        return lista;
//    }
    public List<String> ReporteCantidadxNumdoc(int numero, int idcliente) throws SQLException {
        List<String> lista = new LinkedList<>();
        String sql = " Select concat(primerNombre, ' ', primerApellido, ' ', segundoApellido) as nombre, fechaServicio, vehiculo.placa,  sum(idSERVICIO = 1) as Lavado,  "
                + " sum(idSERVICIO = 2) as Polichado, sum(idSERVICIO = 3) as Cambio_de_Aceite, sum(idSERVICIO = 4) as Balanceo, sum(idSERVICIO = 5) as Alineacion,  "
                + " group_concat(' ',nombreServicio) as Servicios  from servicios_prestados inner join vehiculo on idVEHICULO=FK_idVEHICULO   "
                + " inner join servicio on idservicio=fk_idservicio  inner join cliente where numeroDocumento = ? and FK_idCLIENTE = ?   group by FK_idVEHICULO ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        ps1.setInt(1, numero);
        ps1.setInt(2, idcliente);

        ResultSet rs = ps1.executeQuery();

        String str1 = "";
        String str2 = "";
        String str3 = "";
        String str4 = "";
        String str5 = "";
        String str6 = "";
        String str7 = "";
        String str8 = "";
        String str9 = "";
        while (rs.next()) {
            str1 = rs.getString(1);
            str2 = rs.getString(2);
            str3 = rs.getString(3);
            str4 = rs.getString(4);
            str5 = rs.getString(5);
            str6 = rs.getString(6);
            str7 = rs.getString(7);
            str8 = rs.getString(8);
            str9 = rs.getString(9);
            lista.add(str1);
            lista.add(str2);
            lista.add(str3);
            lista.add(str4);
            lista.add(str5);
            lista.add(str6);
            lista.add(str7);
            lista.add(str8);
            lista.add(str9);
        }

        ps1.close();
        return lista;
    }

    public List<String> ReporteCantidadxPlaca(String placa) throws SQLException {
        List<String> lista = new LinkedList<>();
        String sql = " Select  fechaServicio, vehiculo.placa,  sum(idSERVICIO = 1) as Lavado,  "
                + "sum(idSERVICIO = 2) as Polichado, sum(idSERVICIO = 3) as Cambio_de_Aceite, sum(idSERVICIO = 4) as Balanceo, sum(idSERVICIO = 5) as Alineacion,  "
                + " group_concat(' ',nombreServicio) as Servicios  from servicios_prestados inner join vehiculo on idVEHICULO=FK_idVEHICULO  "
                + " inner join servicio on idservicio=fk_idservicio   where placa = ?  group by FK_idVEHICULO ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        ps1.setString(1, placa);

        ResultSet rs = ps1.executeQuery();

        String str1 = "";
        String str2 = "";
        String str3 = "";
        String str4 = "";
        String str5 = "";
        String str6 = "";
        String str7 = "";
        String str8 = "";
   
        while (rs.next()) {
            str1 = rs.getString(1);
            str2 = rs.getString(2);
            str3 = rs.getString(3);
            str4 = rs.getString(4);
            str5 = rs.getString(5);
            str6 = rs.getString(6);
            str7 = rs.getString(7);
            str8 = rs.getString(8);
          
            lista.add(str1);
            lista.add(str2);
            lista.add(str3);
            lista.add(str4);
            lista.add(str5);
            lista.add(str6);
            lista.add(str7);
            lista.add(str8);
           
        }

        ps1.close();
        return lista;
    }

}
