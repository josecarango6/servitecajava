package Entidades;

import Entidades.ServiciosPrestados;
import Entidades.TipoVehiculo;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-09T17:04:40")
@StaticMetamodel(Vehiculo.class)
public class Vehiculo_ { 

    public static volatile SingularAttribute<Vehiculo, String> tipoVehiculo;
    public static volatile SingularAttribute<Vehiculo, ? extends Object> fKidCLIENTE;
    public static volatile SingularAttribute<Vehiculo, TipoVehiculo> fKidTIPOVEHICULO;
    public static volatile ListAttribute<Vehiculo, ServiciosPrestados> serviciosPrestadosList;
    public static volatile SingularAttribute<Vehiculo, Integer> idVEHICULO;
    public static volatile SingularAttribute<Vehiculo, String> placa;

}