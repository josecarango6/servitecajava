package Entidades;

import java.sql.Date;

public class Vehiculo  {

    private String placa;
    private  Date fechaServicio;
    private String horaIngreso;
    private int FK_idTIPO_VEHICULO;
    private int FK_idCLIENTE;
  

    public Vehiculo() {
    }

    public Vehiculo(String placa, Date fechaServicio, String horaIngreso, int FK_idTIPO_VEHICULO, int FK_idCLIENTE) {
        this.placa = placa;
        this.fechaServicio = fechaServicio;
        this.horaIngreso = horaIngreso;
        this.FK_idTIPO_VEHICULO = FK_idTIPO_VEHICULO;
        this.FK_idCLIENTE = FK_idCLIENTE;
        
    }





    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Date getFechaServicio() {
        return fechaServicio;
    }

    public void setFechaServicio(Date fechaServicio) {
        this.fechaServicio = fechaServicio;
    }

    public String getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(String horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public int getFK_idTIPO_VEHICULO() {
        return FK_idTIPO_VEHICULO;
    }

    public void setFK_idTIPO_VEHICULO(int FK_idTIPO_VEHICULO) {
        this.FK_idTIPO_VEHICULO = FK_idTIPO_VEHICULO;
    }

    public int getFK_idCLIENTE() {
        return FK_idCLIENTE;
    }

    public void setFK_idCLIENTE(int FK_idCLIENTE) {
        this.FK_idCLIENTE = FK_idCLIENTE;
    }




    
}
