
package Entidades;

public class Descuento  {

 
    private int cantidadServicios;
    private int porcentajeDescuento;


    public Descuento() {
        
    }

    public Descuento(int cantidadServicios, int porcentajeDescuento) {
        this.cantidadServicios = cantidadServicios;
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public int getCantidadServicios() {
        return cantidadServicios;
    }

    public void setCantidadServicios(int cantidadServicios) {
        this.cantidadServicios = cantidadServicios;
    }

    public int getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(int porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }



    
}
