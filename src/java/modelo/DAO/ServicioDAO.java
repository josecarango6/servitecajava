package modelo.DAO;

import Dbutil.Database;
import Entidades.Servicio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServicioDAO {

    private Connection conexion;

    public ServicioDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> obtenerTodosLosServicios() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "SELECT  nombreServicio from servicio order by nombreServicio asc";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String nombre = rs.getString(1);

                listaTipos.add(nombre);
            }
        }
        return listaTipos;
    }

    public List<String> obtenerTodosLosServiciosconcosto() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "SELECT  * from servicio order by nombreServicio asc";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String nombre = rs.getString(2);
                String costo1 = rs.getString(3);
                String costo2 = rs.getString(4);
                String costo3 = rs.getString(5);
                String costo4 = rs.getString(6);

                listaTipos.add(nombre);
                listaTipos.add(costo1);
                listaTipos.add(costo2);
                listaTipos.add(costo3);
                listaTipos.add(costo4);
            }
        }
        return listaTipos;
    }

    public void modificar(String concatenado, String nombre) {
        try {
            String sql = "UPDATE servicio "
                    + " SET " + concatenado + " "
                    + " WHERE nombreservicio = ? ";

            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void borrar(String nombre) {
        try {
            String sql = " delete from servicio "
                    + " WHERE nombreservicio = ?";

            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setString(1, nombre);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void guardar(Servicio u) {
        try {
            String sql = " INSERT INTO servicio ( nombreServicio, costoAutomovil, costoCampero, costoCamion, costoBus ) "
                    + " VALUES(?,?,?,?,?)";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setString(1, u.getNombreServicio());
            ps.setInt(2, u.getCostoAutomovil());
            ps.setInt(3, u.getCostoCampero());
            ps.setInt(4, u.getCostoCamion());
            ps.setInt(5, u.getCostoBus());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public List<String> obtenerPlacas() throws SQLException {
        List<String> listaPlacas = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "select DISTINCT placa from vehiculo order by placa asc";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String placas = rs.getString(1);

                listaPlacas.add(placas);
            }
        }
        return listaPlacas;
    }
        
                public List<String> obtenerDocumentos() throws SQLException {
        List<String> listaDocumentos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "select numeroDocumento from cliente";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String documentos = rs.getString(1);

                listaDocumentos.add(documentos);
            }
        }
        return listaDocumentos;
    }
}
