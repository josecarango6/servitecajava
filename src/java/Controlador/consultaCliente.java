
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.ClienteDAO;

/**
 *
 * @author jooss
 */
@WebServlet(name = "consultaCliente", urlPatterns = {"/consultaCliente"})
public class consultaCliente extends HttpServlet {

    public static String valor = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet consultaCliente</title>");
            out.println("</head>");
            out.println("<body>");
            int cedula = 0;
            if (!request.getParameter("cedula").equals("")) {
                cedula = Integer.parseInt(request.getParameter("cedula"));
            }

            //metodo consulta imprime un mensaje si esta registrado o no
            ClienteDAO cldao = new ClienteDAO();
            int val = cldao.ConsultarnumeroDocumento(cedula);
            if (val == 0 && !request.getParameter("cedula").equals("")) {
                valor = String.valueOf(cedula);
                out.print("<h2 class='text-white'>El cliente con numero de documento.  NO se encuentra registrado  </h2>");
                

            }
            if (val != 0) {
                out.print("<h2 class='text-white'>El cliente con numero de documento.  YA se encuentra registrado  </h2>");
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            Logger.getLogger(consultaCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
