package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class UsuarioDAO {

    private Connection conexion;

    public UsuarioDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> Tipousuario(int tipousuario) throws SQLException {
        List<String> lista = new LinkedList<>();
        String sql = " select usuario,contraseña,fk_idtipo_usuario from usuario where FK_idTipo_Usuario = ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        ps1.setInt(1, tipousuario);

        ResultSet rs = ps1.executeQuery();

        String str1 = "";
         String str2 = "";
          String str3 = "";

        while (rs.next()) {
            str1 = rs.getString(1);
            str2 = rs.getString(2);
            str3 = rs.getString(3);
        }
        lista.add(str1);
        lista.add(str2);
        lista.add(str3);

        ps1.close();
        return lista;
    }
        public List<String> Permisodeusuario(int tipousuario) throws SQLException {
        List<String> lista = new LinkedList<>();
        String sql=" select nombreModulo from permisos inner join modulos on idmodulos=FK_idMODULOS where FK_idTIPO_USUARIO = ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);
        ps1.setInt(1, tipousuario);

        ResultSet rs = ps1.executeQuery();

        String str1 = "";
         

        while (rs.next()) {
            str1 = rs.getString(1);
           lista.add(str1);
        }
       
  

        ps1.close();
        return lista;
    }
    

}
