package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.text.NumberFormat;
import java.util.Locale;
import modelo.DAO.LiquidacionDAO;
import modelo.DAO.EstadoLiquidacionDAO;
import modelo.DAO.ServiciosPrestadosDAO;
import modelo.DAO.ServicioDAO;
import java.util.LinkedList;
import java.util.List;
import static Controlador.traedatos.*;

public final class liquidacion_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/loginLogout.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write("<!doctype html> \n");
      out.write("<html lang=\"en\"> \n");
      out.write("    ");

        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.

      out.write(" \n");
      out.write("    <head> \n");
      out.write("        <meta charset=\"utf-8\"> \n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"> \n");
      out.write("        <meta name=\"description\" content=\"\"> \n");
      out.write("        <meta name=\"author\" content=\"\"> \n");
      out.write("        <title>Dashboard Template for Bootstrap</title>         \n");
      out.write("        <!-- Bootstrap core CSS -->         \n");
      out.write("        <link href=\"bootstrap/css/bootstrap.css\" rel=\"stylesheet\"> \n");
      out.write("        <!-- Custom styles for this template -->         \n");
      out.write("        <link href=\"dashboard.css\" rel=\"stylesheet\"> \n");
      out.write("        <!--clockpicker-->         \n");
      out.write("        <script src=\"//code.jquery.com/jquery-1.11.0.min.js\"></script>         \n");
      out.write("        <script src=\"https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js\"></script>         \n");
      out.write("        <link rel=\"stylesheet\" href=\"https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css\"> \n");
      out.write("        <!--clockpicker-->         \n");
      out.write("    </head>     \n");
      out.write("    <body onload=\"carga\"> \n");
      out.write("        <nav class=\"navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow\"> \n");
      out.write("            <a class=\"navbar-brand col-sm-3 mr-0 col-md-2\" href=\"#\">Service Station System</a> \n");
      out.write("            <ol class=\"breadcrumb m-auto p-auto bg-dark\"> \n");
      out.write("                <li class=\"breadcrumb-item\">\n");
      out.write("                    <a href=\"index.jsp\">Inicio</a>\n");
      out.write("                </li>                 \n");
      out.write("                <li class=\"breadcrumb-item active text-white\">Liquidaciï¿½n</li>                 \n");
      out.write("            </ol>             \n");
      out.write("            <ul class=\"navbar-nav px-3\"> \n");
      out.write("                <li class=\"nav-item text-nowrap\"> \n");
      out.write("                    ");
      out.write("<div id=\"lineaLoginLogout\" style=\"color:white\">\n");
      out.write("    ");

            String tipoUsuario = (String) session.getAttribute("tipoUsuario");
            if (tipoUsuario==null) {
               RequestDispatcher vista = request.getRequestDispatcher("login.jsp");
            vista.forward(request, response);
                    
                }

            if (tipoUsuario == null) {
                out.print("<a  href='login.jsp'>Iniciar Sesion</a>");
            } else {
                String nombreUsuario = (String) session.getAttribute("nombreUsuario");
                out.print("<ol><h6>Hola " + nombreUsuario + "      <a   href='ControladorLogin?Salir=logout'>Salir</a></ol></h6> ");
            }
          
        
      out.write(" \n");
      out.write("</div>\n");
      out.write("\n");
      out.write("                </li>                 \n");
      out.write("            </ul>             \n");
      out.write("        </nav>         \n");
      out.write("        <div class=\"container-fluid\"> \n");
      out.write("            <div class=\"row\"> \n");
      out.write("                <nav class=\"col-md-2 d-none d-md-block bg-light sidebar\"> \n");
      out.write("                    <div class=\"sidebar-sticky\"> \n");
      out.write("                        <ul class=\"nav flex-column\"> \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link active\" href=\"index.jsp\" target=\"\"> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewbox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-home\"> \n");
      out.write("                                    <path d=\"M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z\"></path>                                         \n");
      out.write("                                    <polyline points=\"9 22 9 12 15 12 15 22\"></polyline>                                         \n");
      out.write("                                    </svg>                                Inicio <span class=\"sr-only\">(current)</span> </a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"liquidacion.jsp\" target=\"\"> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewbox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-file\"> \n");
      out.write("                                    <path d=\"M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z\"></path>                                         \n");
      out.write("                                    <polyline points=\"13 2 13 9 20 9\"></polyline>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Liquidaciï¿½n </a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"registroVehiculo.jsp\" target=\"\"> <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" aria-hidden=\"true\" focusable=\"false\" width=\"1em\" height=\"1em\" style=\"-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);\" preserveaspectratio=\"xMidYMid meet\" viewbox=\"0 0 11 11\"> \n");
      out.write("                                    <path d=\"M9 4l-.89-2.66A.5.5 0 0 0 7.64 1H3.36a.5.5 0 0 0-.47.34L2 4a1 1 0 0 0-1 1v3h1v1a1 1 0 1 0 2 0V8h3v1a1 1 0 1 0 2 0V8h1V5a1 1 0 0 0-1-1zM3 7a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0-3l.62-2h3.76L8 4H3zm5 3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z\" fill=\"#626262\"></path>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Registro Vehiculo </a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"registroCliente.jsp\"> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewbox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-users\"> \n");
      out.write("                                    <path d=\"M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2\"></path>                                         \n");
      out.write("                                    <circle cx=\"9\" cy=\"7\" r=\"4\"></circle>                                         \n");
      out.write("                                    <path d=\"M23 21v-2a4 4 0 0 0-3-3.87\"></path>                                         \n");
      out.write("                                    <path d=\"M16 3.13a4 4 0 0 1 0 7.75\"></path>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Registro Cliente </a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"servicios.jsp\"> <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" aria-hidden=\"true\" focusable=\"false\" width=\"1em\" height=\"1em\" style=\"-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);\" preserveaspectratio=\"xMidYMid meet\" viewbox=\"0 0 20 20\"> \n");
      out.write("                                    <path d=\"M4 5H.78c-.37 0-.74.32-.69.84l1.56 9.99S3.5 8.47 3.86 6.7c.11-.53.61-.7.98-.7H10s-.7-2.08-.77-2.31C9.11 3.25 8.89 3 8.45 3H5.14c-.36 0-.7.23-.8.64C4.25 4.04 4 5 4 5zm4.88 0h-4s.42-1 .87-1h2.13c.48 0 1 1 1 1zM2.67 16.25c-.31.47-.76.75-1.26.75h15.73c.54 0 .92-.31 1.03-.83.44-2.19 1.68-8.44 1.68-8.44.07-.5-.3-.73-.62-.73H16V5.53c0-.16-.26-.53-.66-.53h-3.76c-.52 0-.87.58-.87.58L10 7H5.59c-.32 0-.63.19-.69.5 0 0-1.59 6.7-1.72 7.33-.07.37-.22.99-.51 1.42zM15.38 7H11s.58-1 1.13-1h2.29c.71 0 .96 1 .96 1z\" fill=\"#626262\"></path>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Servicios </a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"descuento.jsp\"> <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" aria-hidden=\"true\" focusable=\"false\" width=\"1em\" height=\"1em\" style=\"-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);\" preserveaspectratio=\"xMidYMid meet\" viewbox=\"0 0 1024 1024\"> \n");
      out.write("                                    <path d=\"M855.7 210.8l-42.4-42.4a8.03 8.03 0 0 0-11.3 0L168.3 801.9a8.03 8.03 0 0 0 0 11.3l42.4 42.4c3.1 3.1 8.2 3.1 11.3 0L855.6 222c3.2-3 3.2-8.1.1-11.2zM304 448c79.4 0 144-64.6 144-144s-64.6-144-144-144-144 64.6-144 144 64.6 144 144 144zm0-216c39.7 0 72 32.3 72 72s-32.3 72-72 72-72-32.3-72-72 32.3-72 72-72zm416 344c-79.4 0-144 64.6-144 144s64.6 144 144 144 144-64.6 144-144-64.6-144-144-144zm0 216c-39.7 0-72-32.3-72-72s32.3-72 72-72 72 32.3 72 72-32.3 72-72 72z\" fill=\"#626262\"></path>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Descuentos</a>\n");
      out.write("                                <a class=\"nav-link\" href=\"turno.jsp\"> <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" aria-hidden=\"true\" focusable=\"false\" width=\"1em\" height=\"1em\" style=\"-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);\" preserveaspectratio=\"xMidYMid meet\" viewbox=\"0 0 24 24\">\n");
      out.write("                                    <path opacity=\".3\" d=\"M5 19h14V5H5v14zm2.41-7.41L10 14.17l6.59-6.59L18 9l-8 8l-4-4l1.41-1.41z\" fill=\"#626262\"/>\n");
      out.write("                                    <path d=\"M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04a2.008 2.008 0 0 0-1.44 1.19c-.1.24-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55c.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75s-.75-.34-.75-.75s.34-.75.75-.75zM19 19H5V5h14v14z\" fill=\"#626262\"/>\n");
      out.write("                                    </svg> \n");
      out.write("                                    Turno</a> \n");
      out.write("                            </li>                             \n");
      out.write("                        </ul>                         \n");
      out.write("                        <h6 class=\"sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted\"> <span class=\"text-primary\">Reportes</span> <a class=\"d-flex align-items-center text-muted\" href=\"#\"> </a> </h6> \n");
      out.write("                        <ul class=\"nav flex-column mb-2\"> \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"serviciosPrestados.jsp\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewbox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-file-text\"> \n");
      out.write("                                    <path d=\"M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z\"></path>                                         \n");
      out.write("                                    <polyline points=\"14 2 14 8 20 8\"></polyline>                                         \n");
      out.write("                                    <line x1=\"16\" y1=\"13\" x2=\"8\" y2=\"13\"></line>                                         \n");
      out.write("                                    <line x1=\"16\" y1=\"17\" x2=\"8\" y2=\"17\"></line>                                         \n");
      out.write("                                    <polyline points=\"10 9 9 9 8 9\"></polyline>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Cantidad de Servicios Prestados y Tiempo de Atenciï¿½n</a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                                <a class=\"nav-link\" href=\"serviciosPrestadosCV.jsp\"> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewbox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-file-text\"> \n");
      out.write("                                    <path d=\"M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z\"></path>                                         \n");
      out.write("                                    <polyline points=\"14 2 14 8 20 8\"></polyline>                                         \n");
      out.write("                                    <line x1=\"16\" y1=\"13\" x2=\"8\" y2=\"13\"></line>                                         \n");
      out.write("                                    <line x1=\"16\" y1=\"17\" x2=\"8\" y2=\"17\"></line>                                         \n");
      out.write("                                    <polyline points=\"10 9 9 9 8 9\"></polyline>                                         \n");
      out.write("                                    </svg> \n");
      out.write("                                    Servicios Prestados por Cliente o Automotor </a> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                            </li>                             \n");
      out.write("                            <li class=\"nav-item\"> \n");
      out.write("                            </li>                             \n");
      out.write("                        </ul>                         \n");
      out.write("                    </div>                     \n");
      out.write("                </nav>                 \n");
      out.write("                <main role=\"main\" class=\"col-md-9 ml-sm-auto col-lg-10 px-4 mb-5\"> \n");
      out.write("                    <div class=\"chartjs-size-monitor\" style=\"position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;\"> \n");
      out.write("                        <div class=\"chartjs-size-monitor-expand\" style=\"position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;\"> \n");
      out.write("                            <div style=\"position:absolute;width:1000000px;height:1000000px;left:0;top:0\"></div>                             \n");
      out.write("                        </div>                         \n");
      out.write("                        <div class=\"chartjs-size-monitor-shrink\" style=\"position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;\"> \n");
      out.write("                            <div style=\"position:absolute;width:200%;height:200%;left:0; top:0\"></div>                             \n");
      out.write("                        </div>                         \n");
      out.write("                    </div>                     \n");
      out.write("                    <div class=\"d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom\"> \n");
      out.write("                        <h1 class=\"h2\">Liquidacion de Servicios</h1> \n");
      out.write("                        <div class=\"btn-toolbar mb-2 mb-md-0\"> \n");
      out.write("                            <div class=\"btn-group mr-2\"> \n");
      out.write("                                <button class=\"btn btn-sm btn-outline-secondary\">Share</button>                                 \n");
      out.write("                                <button class=\"btn btn-sm btn-outline-secondary\">Export</button>                                 \n");
      out.write("                            </div>                             \n");
      out.write("                            <button class=\"btn btn-sm btn-outline-secondary dropdown-toggle\"> \n");
      out.write("                                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewbox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-calendar\"> \n");
      out.write("                                <rect x=\"3\" y=\"4\" width=\"18\" height=\"18\" rx=\"2\" ry=\"2\"></rect>                                     \n");
      out.write("                                <line x1=\"16\" y1=\"2\" x2=\"16\" y2=\"6\"></line>                                     \n");
      out.write("                                <line x1=\"8\" y1=\"2\" x2=\"8\" y2=\"6\"></line>                                     \n");
      out.write("                                <line x1=\"3\" y1=\"10\" x2=\"21\" y2=\"10\"></line>                                     \n");
      out.write("                                </svg>                                 \n");
      out.write("                                This week\n");
      out.write("                            </button>                             \n");
      out.write("                        </div>                         \n");
      out.write("                    </div>                     \n");
      out.write("                    <div class=\"row\"> \n");
      out.write("                        <div class=\"order-md-1 text-left bg-white pr-5 pb-5 pl-5 col-md-12\"> \n");
      out.write("                            <h4 class=\"mb-3 mt-auto\">Datos del Vehiculo</h4> \n");
      out.write("                            ");
                                String turnodejsp = request.getParameter("turno");
                                String placadejsp = request.getParameter("placa");

                                if (turnodejsp != null && placadejsp != null) {
                                    val3 = placadejsp;
                                    turno = Integer.parseInt(turnodejsp);
      out.write("\n");
      out.write("                            <script>\n");
      out.write("                                var turnojas =");
out.print(turnodejsp);
      out.write(";\n");
      out.write("                                var placajas =");
out.print(placadejsp);
      out.write(";\n");
      out.write("                            </script>\n");
      out.write("                            ");
}
      out.write("\n");
      out.write("                            <script>\n");
      out.write("                                function carga() {\n");
      out.write("\n");
      out.write("                                    if (turnojas !== \"undefined\" && placajas !== \"undefined\") {\n");
      out.write("                                        document.getElementById(\"consultare\").click();\n");
      out.write("                                    }\n");
      out.write("\n");
      out.write("                                }\n");
      out.write("\n");
      out.write("                            </script>\n");
      out.write("                            <form class=\"needs-validation\" novalidate=\"\" action=\"registroLiquidacion\" method=\"post\"> \n");
      out.write("                                <div class=\"row\"> \n");
      out.write("                                    <div class=\"mb-3 col-md-3\">Placa del Vehiculo\n");
      out.write("                                        <br> \n");
      out.write("                                        <input type=\"text\" class=\"form-control\" id=\"placa\" placeholder=\"\" value=\"");
if (val3 != null) {
                                                out.print(val3);
                                            }
                                               
      out.write("\" required=\"\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid first name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"mb-3 col-md-3\">Turno\n");
      out.write("                                        <br> \n");
      out.write("                                        <input type=\"text\" class=\"form-control\" id=\"turno\" placeholder=\"\" value=\"");
if (turno != 0) {
                                                out.print(turno);
                                            }
                                               
      out.write("\" required=\"\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid first name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <!--BOTON DE CONSULTA-->                                     \n");
      out.write("                                    <input class=\"btn btn-primary btn-lg btn-block mb-5 m-auto col-md-3\" onclick=\"window.frames['ventana_iframe'].document.getElementById('automatico').click();\n");
      out.write("                                            location.reload();\" type=\"button\" value=\"Consultar\" id=\"consultare\"> \n");
      out.write("                                    <iframe style=\"display: none\" d='frm1' src='traedatos' marginwidth='0' marginheight='0' name='ventana_iframe' scrolling='no' border='0' frameborder='0' width='300' height='200'> \n");
      out.write("                                    </iframe>                                     \n");
      out.write("                                    <!--este consulta-->                                     \n");
      out.write("                                    <div class=\"invalid-feedback\"> \n");
      out.write("                                        Valid last name is required.\n");
      out.write("                                    </div>                                     \n");
      out.write("                                </div>                                 \n");
      out.write("                                <hr class=\"bg-primary\"> \n");
      out.write("                                <div class=\"row p-auto mr-auto ml-auto mb-2 mt-2\"> \n");
      out.write("                                    <h4 class=\"m-5 m-auto col-md-4\">Servicios Realizados</h4> \n");
      out.write("                                    <h4 class=\"m-auto col-md-4\">Costo del Servicio</h4> \n");
      out.write("                                    <h4 class=\"m-auto col-md-4\">Costo Total Servicioso</h4> \n");
      out.write("                                </div>                                 \n");
      out.write("                                <div class=\"row\"> \n");
      out.write("                                    <div class=\"btn-group-vertical col-md-4 \" data-toggle=\"buttons\"> \n");
      out.write("                                        ");
//trae del servlet traedatos

                                            //prueba
                                            List<Integer> lstPRESTADOS = new LinkedList();
                                            List<String> lstPRESTADOS2 = new LinkedList();

                                            ServiciosPrestadosDAO buscaid = new ServiciosPrestadosDAO();
                                            ServiciosPrestadosDAO busca = new ServiciosPrestadosDAO();
                                            //tipo vehiculo

                                            String tipoveh = "";
                                            LiquidacionDAO ConsultaID = new LiquidacionDAO();
                                            int idvehiculo = ConsultaID.consultaIdVehiculo(turno, val3);

                                            //id vehiculo
                                            tipoveh = busca.conTipoVehiculoSelect(turno, val3);
                                            lstPRESTADOS = buscaid.conServiSelect(turno, val3);
                                            //setiar
                                            turno = 0;

                                            val3 = "";

                                            String captura = "";

                                            for (int i = 0; i < lstPRESTADOS.size(); i++) {

                                                captura = busca.obtenerNombreServicio(lstPRESTADOS.get(i));
                                                lstPRESTADOS2.add(captura);

                                            }
                                            java.util.Collections.sort(lstPRESTADOS2);//ordena alfabeticamente
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {
      out.write(" \n");
      out.write("                                        <label class=\"btn btn-secondary\" for=\"");
out.print(lstPRESTADOS2.get(i));
      out.write("\"> \n");
      out.write("                                            <input type=\"checkbox\" checked=\"\" disabled=\"disabled\" value=\"");
out.print(lstPRESTADOS2.get(i));
      out.write("\" autocomplete=\"off\" d=\"");
out.print(lstPRESTADOS2.get(i));
      out.write("\" name=\"servicio\"> \n");
      out.write("                                            ");
out.print(lstPRESTADOS2.get(i));
                                            
      out.write(" \n");
      out.write("                                        </label>                                             \n");
      out.write("                                        ");
}
      out.write(" \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"col-md-6 col-lg-4\"> \n");
      out.write("                                        ");


                                            List<String> listacostos = new LinkedList();
                                            List<Integer> listacostosformato = new LinkedList();
                                            String captura2 = "";
                                            int capturanumFormato = 0;
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {
                                                captura2 = busca.selectcostostr(tipoveh, lstPRESTADOS2.get(i));
                                                listacostos.add(captura2);
                                                capturanumFormato = busca.selectcostoFrmat(tipoveh, lstPRESTADOS2.get(i));
                                                listacostosformato.add(capturanumFormato);
                                            }
      out.write(" \n");
      out.write("                                        ");
 NumberFormat nf = NumberFormat.getInstance();// le da formato al numero
                                            nf = NumberFormat.getIntegerInstance();

                                            int sumacostos = 0;
                                            int contarServicios = 0;
                                            for (int i = 0; i < listacostos.size(); i++) {
      out.write(" \n");
      out.write("                                        <button class=\"form-control mb-2\" id=\"");
out.print(listacostos.get(i));
      out.write("\" disabled=\"disabled\"> \n");
      out.write("                                            ");
out.print(nf.format(listacostosformato.get(i)));
      out.write(" \n");
      out.write("                                            ");
sumacostos = sumacostos + Integer.parseInt(listacostos.get(i));
      out.write(" \n");
      out.write("                                            ");
contarServicios = contarServicios + 1;
      out.write(" \n");
      out.write("                                        </button>                                             \n");
      out.write("                                        ");
}

                                        
      out.write(" \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"col-md-4 mb-0\"> \n");
      out.write("                                        <input type=\"text\" class=\"form-control mb-5\" id=\"costoServicios\" name=\"costoServicios\" placeholder=\"\" value=\"");
out.print(sumacostos);
      out.write("\" required=\"\" readonly=\"readonly\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid last name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                </div>                                 \n");
      out.write("                                <div class=\"invalid-feedback\"> \n");
      out.write("                                    Valid first name is required.\n");
      out.write("                                </div>                                 \n");
      out.write("                                <div class=\"row p-auto mt-3\"> \n");
      out.write("                                    <div class=\"col-md-6 col-lg-4 m-auto\"> \n");
      out.write("                                        <h4 class=\"p-auto ml-auto mb-auto mr-auto mt-auto ml-3 mb-3 mr-3 m-auto\">Descuento %</h4> \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"col-md-6 col-lg-4 mt-auto mb-auto\">Porcetaje de descuento\n");
      out.write("                                        <br> \n");
      out.write("                                        ");
LiquidacionDAO liqui = new LiquidacionDAO();
                                            int cant = liqui.cantidadServicio();
                                            int porc = 0;
                                            int totalLiquidacion = 0;
                                            if (contarServicios >= cant) {
                                                porc = liqui.PorcentajeDesc();
                                                totalLiquidacion = (sumacostos * porc) / 100;
                                                totalLiquidacion = sumacostos - totalLiquidacion;
                                            }
                                            if (contarServicios < cant) {
                                                totalLiquidacion = sumacostos;
                                            }

                                        
      out.write(" \n");
      out.write("                                        <input type=\"text\" class=\"form-control\" id=\"porcentaje\" name=\"porcentaje\" placeholder=\"\" value=\"");
out.print(porc);
      out.write("\" required=\"\" readonly=\"readonly\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid first name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"mb-3 col-md-4\"> \n");
      out.write("                                        <h4 class=\"m-auto\">Valor Total a Pagar</h4> \n");
      out.write("                                        <input type=\"text\" class=\"form-control\" id=\"totalLiquidacion\" name=\"totalLiquidacion\" placeholder=\"\" value=\"");
out.print(totalLiquidacion);
      out.write("\" required=\"\" readonly=\"readonly\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid last name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                </div>                                 \n");
      out.write("                                <div class=\"row\"> \n");
      out.write("\n");
      out.write("                                    <div style=\"display: none\" lass=\"col-md-6 mb-3\">idvehiculo\n");
      out.write("                                        <br> \n");
      out.write("                                        <input type=\"text\" class=\"form-control\" id=\"idVehiculo\" placeholder=\"\" name=\"idVehiculo\" value=\"");
out.print(idvehiculo);
      out.write("\" required=\"\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid first name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"mb-3 col-md-4\">Fecha de Salida\n");
      out.write("                                        <br> \n");
      out.write("                                        <input type=\"date\" class=\"form-control\" id=\"fechaSalida\" name=\"fechaSalida\" placeholder=\"\" value=\"\" required=\"\"> \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid first name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"col-md-6 mb-3 col-lg-4\">Hora de Salida\n");
      out.write("                                        <br> \n");
      out.write("                                        <input type=\"text\" class=\"form-control\" id=\"horaSalida\" name=\"horaSalida\" placeholder=\"\" value=\"\" required=\"\" readonly=\"readonly\"> \n");
      out.write("                                        <script>\n");
      out.write("                                            $(\"input[name=horaSalida]\").clockpicker({\n");
      out.write("                                                placement: 'bottom',\n");
      out.write("                                                align: 'left',\n");
      out.write("                                                autoclose: true,\n");
      out.write("                                                default: 'now',\n");
      out.write("                                                donetext: \"Select\"\n");
      out.write("                                            });\n");
      out.write("                                        </script>                                         \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid last name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                    <div class=\"mb-3 col-md-3 col-lg-4\">Estado de la Liquidacion\n");
      out.write("                                        <br> \n");
      out.write("                                        ");
 List<String> listaEstados = new LinkedList();
                                            EstadoLiquidacionDAO estado = new EstadoLiquidacionDAO();
                                            listaEstados = estado.obtenerTodosLosEstadosLiquidacion();
      out.write(" \n");
      out.write("                                        <select class=\"custom-select d-block w-100\" required=\"\" id=\"estadoLiquidacion\" name=\"estadoLiquidacion\"> \n");
      out.write("                                            <option value=\"\" disabled selected=\"disabled selected\">seleccione una opcion</option>                                             \n");
      out.write("                                            ");

                                                //LLenandose :)
                                                for (int i = 0; i < listaEstados.size(); i++) {
      out.write(" \n");
      out.write("                                            <option name=\"tipos\"> \n");
      out.write("                                                ");
out.print(listaEstados.get(i));
      out.write(" \n");
      out.write("                                            </option>                                                 \n");
      out.write("                                            ");
  }
      out.write(" \n");
      out.write("                                        </select>                                         \n");
      out.write("                                        <div class=\"invalid-feedback\"> \n");
      out.write("                                            Valid first name is required.\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>                                     \n");
      out.write("                                </div>                                 \n");
      out.write("                                <div class=\"invalid-feedback\"> \n");
      out.write("                                    Valid first name is required.\n");
      out.write("                                </div>                                 \n");
      out.write("                                <hr class=\"mb-4 bg-primary\"> \n");
      out.write("                                <div class=\"row\"> \n");
      out.write("                                    <button class=\"btn btn-primary btn-lg btn-block m-auto col-md-4\" type=\"submit\">Generar Liquidacion</button>                                     \n");
      out.write("                                </div>                                 \n");
      out.write("                            </form>                             \n");
      out.write("                        </div>                         \n");
      out.write("                    </div>                     \n");
      out.write("                </main>                 \n");
      out.write("            </div>             \n");
      out.write("        </div>         \n");
      out.write("        <!-- Bootstrap core JavaScript\n");
      out.write("    ================================================== -->         \n");
      out.write("        <!-- Placed at the end of the document so the pages load faster -->         \n");
      out.write("        <script src=\"assets/js/jquery.min.js\"></script>         \n");
      out.write("        <script src=\"assets/js/popper.js\"></script>         \n");
      out.write("        <script src=\"bootstrap/js/bootstrap.min.js\"></script>         \n");
      out.write("        <!-- Icons -->         \n");
      out.write("        <script src=\"https://unpkg.com/feather-icons/dist/feather.min.js\"></script>         \n");
      out.write("        <script>feather.replace()</script>         \n");
      out.write("        <!-- Graphs -->         \n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js\"></script>         \n");
      out.write("        <script>\n");
      out.write("                                            var ctx = document.getElementById(\"myChart\");\n");
      out.write("                                            var myChart = new Chart(ctx, {\n");
      out.write("                                                type: 'line',\n");
      out.write("                                                data: {\n");
      out.write("                                                    labels: [\"Sunday\", \"Monday\", \"Tuesday\", \"Wednesday\", \"Thursday\", \"Friday\", \"Saturday\"],\n");
      out.write("                                                    datasets: [{\n");
      out.write("                                                            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],\n");
      out.write("                                                            lineTension: 0,\n");
      out.write("                                                            backgroundColor: 'transparent',\n");
      out.write("                                                            borderColor: '#007bff',\n");
      out.write("                                                            borderWidth: 4,\n");
      out.write("                                                            pointBackgroundColor: '#007bff'\n");
      out.write("                                                        }]\n");
      out.write("                                                },\n");
      out.write("                                                options: {\n");
      out.write("                                                    scales: {\n");
      out.write("                                                        yAxes: [{\n");
      out.write("                                                                ticks: {\n");
      out.write("                                                                    beginAtZero: false\n");
      out.write("                                                                }\n");
      out.write("                                                            }]\n");
      out.write("                                                    },\n");
      out.write("                                                    legend: {\n");
      out.write("                                                        display: false,\n");
      out.write("                                                    }\n");
      out.write("                                                }\n");
      out.write("                                            });\n");
      out.write("        </script>         \n");
      out.write("    </body>     \n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
