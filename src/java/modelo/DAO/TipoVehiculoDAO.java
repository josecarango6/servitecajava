
package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class TipoVehiculoDAO {
        private Connection conexion;

    public TipoVehiculoDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> obtenerTodosLosVehiculos() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "SELECT  nombreTipoVehiculo from tipo_vehiculo";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String nombre = rs.getString(1);

                listaTipos.add(nombre);
            }
        }
        return listaTipos;
    }
    
}
