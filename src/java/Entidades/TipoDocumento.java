
package Entidades;

public class TipoDocumento {

    private int idTIPODOCUMENTO;
   
    private String nombreDocumento;
   
    

    public TipoDocumento() {
    }

    public TipoDocumento(int idTIPODOCUMENTO) {
        this.idTIPODOCUMENTO = idTIPODOCUMENTO;
    }

    public TipoDocumento(int idTIPODOCUMENTO, String nombreDocumento) {
        this.idTIPODOCUMENTO = idTIPODOCUMENTO;
        this.nombreDocumento = nombreDocumento;
    }

    public int getIdTIPODOCUMENTO() {
        return idTIPODOCUMENTO;
    }

    public void setIdTIPODOCUMENTO(int idTIPODOCUMENTO) {
        this.idTIPODOCUMENTO = idTIPODOCUMENTO;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }


    
}
