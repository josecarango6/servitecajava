package Dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author perez
 */
public class Database {

    private static Connection conexion = null;

    public static Connection getConexion() throws SQLException, ClassNotFoundException {
//useUnicode=true&characterEncoding=utf-8
//characterEncoding=utf8
       
            Class.forName("com.mysql.jdbc.Driver");  //DRIVER DE SQL DEVELOPER
            String myDB = "jdbc:mysql://localhost:3306/serviteca?characterEncoding=utf8";//URL DE SQL DEVELOPER
            
            //Class.forName("org.apache.derby.jdbc.ClientDriver");//DRIVER DE JAVADB
            //String myDB = "jdbc:derby://localhost:1527/serviteca";//URL DE JAVADB
            String usuario = "serviteca";
            String password = "serviteca2019";
            conexion = DriverManager.getConnection(myDB, usuario, password);
            
            return conexion;
   

    }
}
