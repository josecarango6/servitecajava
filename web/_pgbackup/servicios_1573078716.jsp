<%-- 
    Document   : registroCliente
    Created on : 8/09/2019, 07:11:12 PM
    Author     : jooss
--%> 
<%@page import="static Controlador.servicioOpera.*"%> 
<%@page import="modelo.DAO.DescuentoDAO"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="modelo.DAO.TipoDocumentoDAO"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="Entidades.TipoDocumento"%> 
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@page import=" static Controlador.consultaCliente.*"%> 
<!doctype html> 
<html style="background: linear-gradient(90deg, #4b6cb7, #182848);"> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
    %> 
    <head> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>         
        <title>Registro Cliente</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <link href="bootstrap/css/searchbox.css" rel="stylesheet" type="text/css"/> 
    </head>     
    <body id="cuerpito"> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Servicios</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row" style="background: linear-gradient(90deg, #4b6cb7, #182848);"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mb-5"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Servicios</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <!--cuadro de busqueda-->                                 
                                <div class='search-container'> 
</div>                                 
                                <!--cuadro de busqueda-->                                 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <script>
                        var dato = [];
                        var dato2 = '';

                        $(document).ready(function () {

                            var limite = getBody($('table.Tablefu'));


                            $('#table-list tr').on('click', function () {

                                quitarformato();
                                for (var i = 0; i < limite; i++) {
                                    $(this).find("td:eq(" + i + ")").attr("contenteditable", "true");
                                    $(this).find("td:eq(" + i + ")").css('background-color', 'gray');


                                    if (i === 0) {
                                        document.getElementById("seleccionado").value = $(this).find("td:eq(" + i + ")").html();
                                        dato2 = document.getElementById("seleccionado").value;

                                    }

                                }
                            });
                            $('#table-list tr').on('mouseout', function () {

                                for (var i = 0; i < limite; i++) {
                                    $(this).find("td:eq(" + i + ")").attr("contenteditable", "false");
                                    $(this).find("td:eq(" + i + ")").css('background-color', 'white');



                                }

                            });
                            $('#table-list tr').on('keyup', function () {
                                for (var i = 0; i < limite; i++) {
                                    dato[i] = $(this).find("td:eq(" + i + ")").html();
                                }

                            });
                            $('#table-list').on('mouseout', function () {


                            });
                        });


                        function getBody(element) {

                            var originalTable = element.clone();
                            var tds = $(originalTable).children('tbody').children('tr').length;
                            return tds;
                        }




                    </script>                     
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left pr-5 pl-5 col-md-12 mb-5 pb-5"> 
                            <div class="row"> 
                                <h4 class="mb-3 mt-auto">Costo de los Servicios</h4> 
                            </div>
                            <%  ServicioDAO servic = new ServicioDAO();

                                servic.modificar(concatenado, nombreservicio);
                                concatenado = "";
                                nombreservicio = "";
                            %> 
                            <!--<form class="needs-validation" novalidate="" method="post" action="registroCliente"> -->                             
                            <div class="row"> 
                                <div class="col-md-12 table-content"> 
                                    <table class="Tablefu table table-borded table-light table-bordered" id="table-list"> 
                                        <thead class="table-dark"> 
                                            <tr> 
                                                <%DescuentoDAO ddao = new DescuentoDAO();
                                                    List<String> lista1 = new LinkedList();
                                                    lista1 = ddao.obtenerCabecercostos();
                                                    for (int i = 1; i < lista1.size(); i++) {%> 
                                                    <th><%out.print(lista1.get(i));%></th> 
                                                <%}%> 
                                            </tr>                                             
                                        </thead>                                         
                                            <tbody> 
                                            <%
                                                ServicioDAO servi = new ServicioDAO();
                                                List<String> listaTipos = new LinkedList();
                                                listaTipos = servi.obtenerTodosLosServiciosconcosto();
                                                for (int i = 0; i < listaTipos.size(); i++) {%> 
                                            <tr> 
                                                    <td scope="row"><%out.print(listaTipos.get(i));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 1));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 2));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 3));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 4));%></td> 
                                                    <%i = i + 4;%> 
                                                <%}%> 
                                                </tr>                                                 
                                        </tbody>                                         
                                    </table>                                     
                                    <input style="display: none" id="seleccionado" name="borra" readonly=""> 
                                    <button onclick="window.location.href = 'http://localhost:8080/Serviteca/servicioOpera?dato=' + dato + '&dato2=' + dato2 + '&op=modifica'" class="btn btn-info" id="add"> 
                                        <span class="glyphicon glyphicon-plus-sign"></span>Modificar
                                    </button>                                     
                                    <!-- <button onclick="window.location.href = 'http://localhost:8080/Serviteca/servicioOperaborra?dato2=' + dato2 + '&op=borra'" class="btn btn-info" id="add"> 
                                         <span class="glyphicon glyphicon-plus-sign"></span>Eliminar
                                     </button> -->                                     
                                    <!-- Button trigger modal -->                                     
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal"> 
                                        Agregar
</button>                                     
                                    <!-- Modal -->                                     
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
                                        <div class="modal-dialog" role="document"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <h5 class="modal-title" id="exampleModalLabel">Agregar un Nuevo Servicio</h5> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                                                        <span aria-hidden="true">&times;</span> 
                                                    </button>                                                     
                                                </div>                                                 
                                                <form class="container" id="needs-validation" novalidate method="post" action="registroServicio"> 
                                                    <div class="modal-body"> 
                                                        ..........................................................
                                                        <div class="row col-lg-12"> 
                                                            <div class="col-md-6 mb-3 col-lg-5"> 
                                                                <label for="firstName">Nombre del Servicio</label>                                                                 
                                                                <input type="text" class="form-control" id="nombreServicio" name="nombreServicio" laceholder="" value="" required=""> 
                                                                <div class="invalid-feedback"> 
                                                                    Valid first name is required.
</div>                                                                 
                                                            </div>                                                             
                                                            <div class="col-md-6 mb-3 col-lg-7"> 
                                                                <div class="invalid-feedback"> 
                                                                    Valid first name is required.
</div>                                                                 
                                                            </div>                                                             
                                                            <div class="col-md-6 mb-3 col-lg-6"> 
                                                                <label for="lastName">Costo Automovil</label>                                                                 
                                                                <input type="text" class="form-control" id="costoAutomovil" name="costoAutomovil" placeholder="" value="" required=""> 
                                                                <div class="invalid-feedback"> 
                                                                    Valid last name is required.
</div>                                                                 
                                                            </div>                                                             
                                                            <div class="col-md-6 mb-3 col-lg-6"> 
                                                                <label for="lastName">Costo Campero</label>                                                                 
                                                                <input type="text" class="form-control" id="costoCampero" name="costoCampero" placeholder="" value="" required=""> 
                                                                <div class="invalid-feedback"> 
                                                                    Valid last name is required.
</div>                                                                 
                                                            </div>                                                             
                                                            <div class="col-md-6 mb-3 col-lg-6"> 
                                                                <label for="lastName">Costo Camion</label>                                                                 
                                                                <input type="text" class="form-control" id="costoCamion" name="costoCamion" placeholder="" value="" required=""> 
                                                                <div class="invalid-feedback"> 
                                                                    Valid last name is required.
</div>                                                                 
                                                            </div>                                                             
                                                            <div class="col-md-6 mb-3 col-lg-6"> 
                                                                <label for="lastName">Costo Bus</label>                                                                 
                                                                <input type="text" class="form-control" id="costoBus" name="costoBus" placeholder="" value="" required=""> 
                                                                <div class="invalid-feedback"> 
                                                                    Valid last name is required.
</div>                                                                 
                                                            </div>                                                             
                                                        </div>                                                         
                                                        ..........................................................
                                                    </div>                                                     
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>                                                         
                                                        <button type="submit" class="btn btn-primary">Agregar</button>                                                         
                                                    </div>                                                     
                                                </form>                                                 
                                                <script src="assets/js/jquery.min.js">
                                    <script type="bootstrap/js/bootstrap.min.js" </script>                                                 
                                                <script>
                           // Example starter JavaScript for disabling form submissions if there are invalid fields
                            (function() {
                                            'use strict';
                                    window.addEventListener('load', function() {
                                    var form = document.getElementById('needs-validation');
                                    form.addEventListener('submit', function(event) {
                                    if (form.checkValidity() === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                    }
                            form.classList.add('was-validated');
                            }, false);
                            }, false);
                            })();
                                </script>                                                 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                    <!-- <button onclick="quitarformato()" class="btn btn-info" id="add">
                                        <span class="glyphicon glyphicon-plus-sign"></span>Quitar formato
                                    </button>  -->                                     
                                </div>                                 
                                <script>

                                    document.addEventListener("DOMContentLoaded", function (event) {

                                        darformato();



                                    });

//sin formato decimal
                                    function darformato() {//le da formato decimal a los precios

                                        var valores = document.getElementsByName("costos");
                                        for (var i = 0; i < valores.length; i++) {

                                            if (valores[i].innerHTML !== "") {
                                                valores[i].innerHTML = valores[i].innerHTML
                                                        .replace(/\D/g, "")
                                                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                            }
                                        }
                                    }
                                    function quitarformato() {
                                        var valores = document.getElementsByName("costos");
                                        for (var i = 0; i < valores.length; i++) {

                                            var a = valores[i].innerHTML;
                                            if (a.split(".")[0] !== undefined && a.split(".")[1] !== undefined) {
                                                valores[i].innerHTML = a.split(".")[0] + a.split(".")[1];
                                            }
                                            if (a.split(".")[2] !== undefined && a.split(".")[3] !== undefined) {
                                                valores[i].innerHTML = a.split(".")[2] + a.split(".")[3];
                                            }


                                        }
                                    }





                                </script>                                 
                            </div>                             
                            <hr class="mb-4"> 
                            <!-- </form>   -->                             
                        </div>                         
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
    ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
                                    var ctx = document.getElementById("myChart");
                                    var myChart = new Chart(ctx, {
                                        type: 'line',
                                        data: {
                                            labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                            datasets: [{
                                                    data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                    lineTension: 0,
                                                    backgroundColor: 'transparent',
                                                    borderColor: '#007bff',
                                                    borderWidth: 4,
                                                    pointBackgroundColor: '#007bff'
                                                }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                        ticks: {
                                                            beginAtZero: false
                                                        }
                                                    }]
                                            },
                                            legend: {
                                                display: false,
                                            }
                                        }
                                    }
                                    );
        </script>         
    </body>     
</html>
