<%@page import="java.text.NumberFormat"%> 
<%@page import="java.util.Locale"%> 
<%@page import="modelo.DAO.LiquidacionDAO"%> 
<%@page import="modelo.DAO.EstadoLiquidacionDAO"%> 
<%@page import="modelo.DAO.ServiciosPrestadosDAO"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="java.util.List"%> 
<%@page import=" static Controlador.traedatos.*"%> 
<!doctype html> 
<html lang="en"> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
%> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Dashboard Template for Bootstrap</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <!--clockpicker-->         
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>         
        <script src="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>         
        <link rel="stylesheet" href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css"> 
        <!--clockpicker-->         
    </head>     
    <body onload="carga"> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item">
                    <a href="index.jsp">Inicio</a>
                </li>                 
                <li class="breadcrumb-item active text-white">Liquidaci�n</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %>
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %>                        
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mb-5"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Liquidacion de Servicios</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <div class="row"> 
                        <div class="order-md-1 text-left bg-white pr-5 pb-5 pl-5 col-md-12"> 
                            <h4 class="mb-3 mt-auto">Datos del Vehiculo</h4> 
                            <%                                String turnodejsp = request.getParameter("turno");
                                String placadejsp = request.getParameter("placa");

                                if (turnodejsp != null && placadejsp != null) {
                                    val3 = placadejsp;
                                    turno = Integer.parseInt(turnodejsp);%>
                            <script>
                                var turnojas =<%out.print(turnodejsp);%>;
                                var placajas =<%out.print(placadejsp);%>;
                            </script>
                            <%}%>
                            <script>
                                function carga() {

                                    if (turnojas !== "undefined" && placajas !== "undefined") {
                                        document.getElementById("consultare").click();
                                    }

                                }

                            </script>
                            <form class="needs-validation" novalidate="" action="registroLiquidacion" method="post"> 
                                <div class="row"> 
                                    <div class="mb-3 col-md-3">Placa del Vehiculo
                                        <br> 
                                        <input type="text" class="form-control" id="placa" placeholder="" value="<%if (val3 != null) {
                                                out.print(val3);
                                            }
                                               %>" required=""> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>                                         
                                    </div>                                     
                                    <div class="mb-3 col-md-3">Turno
                                        <br> 
                                        <input type="text" class="form-control" id="turno" placeholder="" value="<%if (turno != 0) {
                                                out.print(turno);
                                            }
                                               %>" required=""> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>                                         
                                    </div>                                     
                                    <!--BOTON DE CONSULTA-->                                     
                                    <input class="btn btn-primary btn-lg btn-block mb-5 m-auto col-md-3" onclick="window.frames['ventana_iframe'].document.getElementById('automatico').click();
                                            location.reload();" type="button" value="Consultar" id="consultare"> 
                                    <iframe style="display: none" d='frm1' src='traedatos' marginwidth='0' marginheight='0' name='ventana_iframe' scrolling='no' border='0' frameborder='0' width='300' height='200'> 
                                    </iframe>                                     
                                    <!--este consulta-->                                     
                                    <div class="invalid-feedback"> 
                                        Valid last name is required.
                                    </div>                                     
                                </div>                                 
                                <hr class="bg-primary"> 
                                <div class="row p-auto mr-auto ml-auto mb-2 mt-2"> 
                                    <h4 class="m-5 m-auto col-md-4">Servicios Realizados</h4> 
                                    <h4 class="m-auto col-md-4">Costo del Servicio</h4> 
                                    <h4 class="m-auto col-md-4">Costo Total Servicioso</h4> 
                                </div>                                 
                                <div class="row"> 
                                    <div class="btn-group-vertical col-md-4 " data-toggle="buttons"> 
                                        <%//trae del servlet traedatos

                                            //prueba
                                            List<Integer> lstPRESTADOS = new LinkedList();
                                            List<String> lstPRESTADOS2 = new LinkedList();

                                            ServiciosPrestadosDAO buscaid = new ServiciosPrestadosDAO();
                                            ServiciosPrestadosDAO busca = new ServiciosPrestadosDAO();
                                            //tipo vehiculo

                                            String tipoveh = "";
                                            LiquidacionDAO ConsultaID = new LiquidacionDAO();
                                            int idvehiculo = ConsultaID.consultaIdVehiculo(turno, val3);

                                            //id vehiculo
                                            tipoveh = busca.conTipoVehiculoSelect(turno, val3);
                                            lstPRESTADOS = buscaid.conServiSelect(turno, val3);
                                            //setiar
                                            turno = 0;

                                            val3 = "";

                                            String captura = "";

                                            for (int i = 0; i < lstPRESTADOS.size(); i++) {

                                                captura = busca.obtenerNombreServicio(lstPRESTADOS.get(i));
                                                lstPRESTADOS2.add(captura);

                                            }
                                            java.util.Collections.sort(lstPRESTADOS2);//ordena alfabeticamente
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {%> 
                                        <label class="btn btn-secondary" for="<%out.print(lstPRESTADOS2.get(i));%>"> 
                                            <input type="checkbox" checked="" disabled="disabled" value="<%out.print(lstPRESTADOS2.get(i));%>" autocomplete="off" d="<%out.print(lstPRESTADOS2.get(i));%>" name="servicio"> 
                                            <%out.print(lstPRESTADOS2.get(i));
                                            %> 
                                        </label>                                             
                                        <%}%> 
                                    </div>                                     
                                    <div class="col-md-6 col-lg-4"> 
                                        <%

                                            List<String> listacostos = new LinkedList();
                                            List<Integer> listacostosformato = new LinkedList();
                                            String captura2 = "";
                                            int capturanumFormato = 0;
                                            for (int i = 0; i < lstPRESTADOS2.size(); i++) {
                                                captura2 = busca.selectcostostr(tipoveh, lstPRESTADOS2.get(i));
                                                listacostos.add(captura2);
                                                capturanumFormato = busca.selectcostoFrmat(tipoveh, lstPRESTADOS2.get(i));
                                                listacostosformato.add(capturanumFormato);
                                            }%> 
                                        <% NumberFormat nf = NumberFormat.getInstance();// le da formato al numero
                                            nf = NumberFormat.getIntegerInstance();

                                            int sumacostos = 0;
                                            int contarServicios = 0;
                                            for (int i = 0; i < listacostos.size(); i++) {%> 
                                        <button class="form-control mb-2" id="<%out.print(listacostos.get(i));%>" disabled="disabled"> 
                                            <%out.print(nf.format(listacostosformato.get(i)));%> 
                                            <%sumacostos = sumacostos + Integer.parseInt(listacostos.get(i));%> 
                                            <%contarServicios = contarServicios + 1;%> 
                                        </button>                                             
                                        <%}

                                        %> 
                                        <div class="invalid-feedback"> 
                                        </div>                                         
                                    </div>                                     
                                    <div class="col-md-4 mb-0"> 
                                        <input type="text" class="form-control mb-5" id="costoServicios" name="costoServicios" placeholder="" value="<%out.print(sumacostos);%>" required="" readonly="readonly"> 
                                        <div class="invalid-feedback"> 
                                            Valid last name is required.
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                                <div class="invalid-feedback"> 
                                    Valid first name is required.
                                </div>                                 
                                <div class="row p-auto mt-3"> 
                                    <div class="col-md-6 col-lg-4 m-auto"> 
                                        <h4 class="p-auto ml-auto mb-auto mr-auto mt-auto ml-3 mb-3 mr-3 m-auto">Descuento %</h4> 
                                    </div>                                     
                                    <div class="col-md-6 col-lg-4 mt-auto mb-auto">Porcetaje de descuento
                                        <br> 
                                        <%LiquidacionDAO liqui = new LiquidacionDAO();
                                            int cant = liqui.cantidadServicio();
                                            int porc = 0;
                                            int totalLiquidacion = 0;
                                            if (contarServicios >= cant) {
                                                porc = liqui.PorcentajeDesc();
                                                totalLiquidacion = (sumacostos * porc) / 100;
                                                totalLiquidacion = sumacostos - totalLiquidacion;
                                            }
                                            if (contarServicios < cant) {
                                                totalLiquidacion = sumacostos;
                                            }

                                        %> 
                                        <input type="text" class="form-control" id="porcentaje" name="porcentaje" placeholder="" value="<%out.print(porc);%>" required="" readonly="readonly"> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>                                         
                                    </div>                                     
                                    <div class="mb-3 col-md-4"> 
                                        <h4 class="m-auto">Valor Total a Pagar</h4> 
                                        <input type="text" class="form-control" id="totalLiquidacion" name="totalLiquidacion" placeholder="" value="<%out.print(totalLiquidacion);%>" required="" readonly="readonly"> 
                                        <div class="invalid-feedback"> 
                                            Valid last name is required.
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                                <div class="row"> 

                                    <div style="display: none" lass="col-md-6 mb-3">idvehiculo
                                        <br> 
                                        <input type="text" class="form-control" id="idVehiculo" placeholder="" name="idVehiculo" value="<%out.print(idvehiculo);%>" required=""> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>                                         
                                    </div>                                     
                                    <div class="mb-3 col-md-4">Fecha de Salida
                                        <br> 
                                        <input type="date" class="form-control" id="fechaSalida" name="fechaSalida" placeholder="" value="" required=""> 
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4">Hora de Salida
                                        <br> 
                                        <input type="text" class="form-control" id="horaSalida" name="horaSalida" placeholder="" value="" required="" readonly="readonly"> 
                                        <script>
                                            $("input[name=horaSalida]").clockpicker({
                                                placement: 'bottom',
                                                align: 'left',
                                                autoclose: true,
                                                default: 'now',
                                                donetext: "Select"
                                            });
                                        </script>                                         
                                        <div class="invalid-feedback"> 
                                            Valid last name is required.
                                        </div>                                         
                                    </div>                                     
                                    <div class="mb-3 col-md-3 col-lg-4">Estado de la Liquidacion
                                        <br> 
                                        <% List<String> listaEstados = new LinkedList();
                                            EstadoLiquidacionDAO estado = new EstadoLiquidacionDAO();
                                            listaEstados = estado.obtenerTodosLosEstadosLiquidacion();%> 
                                        <select class="custom-select d-block w-100" required="" id="estadoLiquidacion" name="estadoLiquidacion"> 
                                            <option value="" disabled selected="disabled selected">seleccione una opcion</option>                                             
                                            <%
                                                //LLenandose :)
                                                for (int i = 0; i < listaEstados.size(); i++) {%> 
                                            <option name="tipos"> 
                                                <%out.print(listaEstados.get(i));%> 
                                            </option>                                                 
                                            <%  }%> 
                                        </select>                                         
                                        <div class="invalid-feedback"> 
                                            Valid first name is required.
                                        </div>                                         
                                    </div>                                     
                                </div>                                 
                                <div class="invalid-feedback"> 
                                    Valid first name is required.
                                </div>                                 
                                <hr class="mb-4 bg-primary"> 
                                <div class="row"> 
                                    <button class="btn btn-primary btn-lg btn-block m-auto col-md-4" type="submit">Generar Liquidacion</button>                                     
                                </div>                                 
                            </form>                             
                        </div>                         
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
    ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
                                            var ctx = document.getElementById("myChart");
                                            var myChart = new Chart(ctx, {
                                                type: 'line',
                                                data: {
                                                    labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                    datasets: [{
                                                            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                            lineTension: 0,
                                                            backgroundColor: 'transparent',
                                                            borderColor: '#007bff',
                                                            borderWidth: 4,
                                                            pointBackgroundColor: '#007bff'
                                                        }]
                                                },
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: false
                                                                }
                                                            }]
                                                    },
                                                    legend: {
                                                        display: false,
                                                    }
                                                }
                                            });
        </script>         
    </body>     
</html>