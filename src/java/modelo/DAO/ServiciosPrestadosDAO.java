package modelo.DAO;

import Dbutil.Database;
import Entidades.ServiciosPrestados;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiciosPrestadosDAO {

    private Connection conexion;

    public ServiciosPrestadosDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public void guardar(ServiciosPrestados u) {
        try {
            String sql = " INSERT INTO Servicios_Prestados (turno,FK_idSERVICIO,FK_idVEHICULO) "
                    + " VALUES(?,?,?)";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setInt(1, u.getTurno());
            ps.setInt(2, u.getfKidSERVICIO());
            ps.setInt(3, u.getfKidVEHICULO());
            //para cambios

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(VehiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenerIdServicio(String nombreServicio) throws SQLException {

        String sql = " SELECT idSERVICIO FROM servicio "
                + " WHERE nombreServicio = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setString(1, nombreServicio);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        return idtipoDoc;
    }

    public String obtenerNombreServicio(int idServicio) throws SQLException {

        String sql = " SELECT nombreServicio FROM servicio  "
                + " WHERE idServicio = ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, idServicio);

        ResultSet rs = ps1.executeQuery();

        String idtipoDoc = "";
        if (rs.next()) {
            idtipoDoc = rs.getString(1);
        }
        return idtipoDoc;
    }

    public int turnoIncrement() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "select turno from servicios_prestados order by idservicios_prestados desc limit 1";
        ResultSet rs = stmt.executeQuery(sql);

        int ultimo = 0;

        if (rs != null && rs.next()) {
            ultimo = rs.getInt(1);
//codigo para tratar al conjunto de registros o al registro obtenido
        } else {
            ultimo = 0;
        }

        stmt.close();
        return ultimo;
    }

    public int LimiteTurno() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "select limiteTurno from servicios_prestados  ORDER BY idSERVICIOS_PRESTADOS  DESC LIMIT 1;";
        ResultSet rs = stmt.executeQuery(sql);

        int ultimo = 0;

        if (rs != null && rs.next()) {
            ultimo = rs.getInt(1);
//codigo para tratar al conjunto de registros o al registro obtenido
        } else {
            ultimo = -1;
        }

        stmt.close();
        return ultimo;
    }

    public List<String> selectcostos(String tipo) throws SQLException {
        List<String> listacostos = new LinkedList<>();

        Statement stmt = conexion.createStatement();
        if (tipo != null) {
            String sql = "SELECT costo" + tipo + " "
                    + " FROM servicio order by nombreServicio asc";

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {

                String costo = rs.getString(1);
                listacostos.add(costo);
            }

            stmt.close();
            return listacostos;
        }
        return null;
    }

    public String selectcostostr(String tipo, String nombreservicio) throws SQLException {

        Statement stmt = conexion.createStatement();
        String sql = "SELECT costo" + tipo + ""
                + " FROM servicio where nombreServicio='" + nombreservicio + "'";
        ResultSet rs = stmt.executeQuery(sql);
        String costo = "";
        while (rs.next()) {

            costo = rs.getString(1);

        }

        stmt.close();
        return costo;
    }

    public int selectcostoFrmat(String tipo, String nombreservicio) throws SQLException {

        Statement stmt = conexion.createStatement();
        String sql = "SELECT costo" + tipo + ""
                + " FROM servicio where nombreServicio='" + nombreservicio + "'";
        ResultSet rs = stmt.executeQuery(sql);
        int costo = 0;
        while (rs.next()) {

            costo = rs.getInt(1);

        }

        stmt.close();
        return costo;
    }

    public List<Integer> conServiSelect(int turno, String placa) throws SQLException {
        List<Integer> listaselect = new LinkedList<>();
        String sql = "select FK_idSERVICIO from servicios_prestados  INNER join "
                + "vehiculo on FK_idVEHICULO=idVEHICULO where estadoTurno='0' and turno = ?  AND placa = ? ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);
     
        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();

        while (rs.next()) {

            int servicio = rs.getInt(1);
            listaselect.add(servicio);
        }

        ps1.close();
        return listaselect;
    }

    public String conTipoVehiculoSelect(int turno, String placa) throws SQLException {

        String sql = "select nombreTipoVehiculo from servicios_prestados  INNER join  "
                + " vehiculo on FK_idVEHICULO=idVEHICULO inner join tipo_vehiculo on "
                + " idTIPO_VEHICULO=FK_idTIPO_VEHICULO  where estadoTurno='0' and turno = ?  AND placa = ? limit 1";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);
       
        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();
        String tipoveh = "";
        while (rs.next()) {

            tipoveh = rs.getString(1);

        }

        ps1.close();
        return tipoveh;
    }

}
