package Controlador;

import Dbutil.Mensajes;
import Entidades.ServiciosPrestados;
import Entidades.Vehiculo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.ClienteDAO;
import modelo.DAO.ServiciosPrestadosDAO;
import modelo.DAO.VehiculoDAO;

@WebServlet(name = "registroVehiculo", urlPatterns = {"/registroVehiculo"})
public class registroVehiculo extends HttpServlet {

    public static int aviso = 0;
    
    public static int turno;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            ClienteDAO dcli = new ClienteDAO();
            String placa = request.getParameter("placa");
            //consulta id del string
            String tipovehiculo = request.getParameter("tipoVehiculo");
//convert to date
            String fechaServicio = request.getParameter("fecha");
            String horaIngreso = request.getParameter("horaingreso") + ":00";

            Date dateFecha = Date.valueOf(fechaServicio);
            //probando buscaturno///////
            VehiculoDAO daovehiculo = new VehiculoDAO();

            int idtipoVeh = daovehiculo.obtenerIdtipoVeh(tipovehiculo);
            int usuarioDefecto = 1;
            //usuario por defecto
            if (!request.getParameter("numeroDocumento").equals("")) {
                usuarioDefecto = Integer.parseInt(request.getParameter("numeroDocumento"));
                int idcliente = dcli.Consultaridcliente(usuarioDefecto);
                if (idcliente != 0) {
                    Vehiculo u = new Vehiculo(placa, dateFecha, horaIngreso, idtipoVeh, usuarioDefecto);
                    daovehiculo.guardar(u);
                } 
            } else {
                Vehiculo u = new Vehiculo(placa, dateFecha, horaIngreso, idtipoVeh, usuarioDefecto);
                daovehiculo.guardar(u);
            }

            //buscando id cliente maximo
            int idVehiculoMAX = daovehiculo.obtenerIdMasGrande();
            //insertano servicios
            String[] nombreservicios = request.getParameterValues("servicio");
//seleccionar el id de cada unos de los nombres de la tabla servicio
            ServiciosPrestadosDAO daoServiPresta = new ServiciosPrestadosDAO();
//vector del mismo tamaño 
            int[] idServicios = new int[nombreservicios.length];//creo que es -1
            //instanciar vector de objectos
            ServiciosPrestados arrayObjetos[] = new ServiciosPrestados[nombreservicios.length];
            int incremento = daoServiPresta.turnoIncrement() + 1;
            int limite = daoServiPresta.LimiteTurno();
            if (limite == -1) {
                incremento = 1;
            }
            if (incremento > limite) {
                incremento = 1;
            }
            for (int i = 0; i < nombreservicios.length; i++) {
                idServicios[i] = daoServiPresta.obtenerIdServicio(nombreservicios[i]);
//amedida que lo obtiene va insertando
                arrayObjetos[i] = new ServiciosPrestados(incremento, idServicios[i], idVehiculoMAX);
//vector de objetos insertando
                daoServiPresta.guardar(arrayObjetos[i]);
            }
            turno = incremento;
            aviso = 1;

            RequestDispatcher vista = request.getRequestDispatcher("/registroVehiculo.jsp");
            vista.forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(registroVehiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
