package modelo.DAO;

import Dbutil.Database;
import Entidades.Descuento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DescuentoDAO {

    private Connection conexion;

    public DescuentoDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public void modificar(Descuento u) {
        try {
            String sql = "UPDATE Descuento "
                    + " SET cantidadServicios = ? ,"
                    + "  porcentajeDescuento = ? "
                    + " WHERE idDESCUENTO = 1 ";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setInt(1, u.getCantidadServicios());
            ps.setInt(2, u.getPorcentajeDescuento());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(DescuentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenercantidad() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "SELECT  cantidadServicios FROM  descuento";
        ResultSet rs = stmt.executeQuery(sql);

        int cantidad = 0;

        // cada vez que se ejecuta el método rs.next(), se avanza el cursor una fila 
        // Cuando se alcalza el fin del cursor, la funcion rs.next() retorna false
        if (rs.next()) {
            cantidad = rs.getInt(1);
        }

        stmt.close();
        return cantidad;
    }

    public int obtenerporcen() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "SELECT  porcentajeDescuento FROM  descuento";
        ResultSet rs = stmt.executeQuery(sql);

        int por = 0;

        // cada vez que se ejecuta el método rs.next(), se avanza el cursor una fila 
        // Cuando se alcalza el fin del cursor, la funcion rs.next() retorna false
        if (rs.next()) {
            por = rs.getInt(1);
        }

        stmt.close();
        return por;
    }

    public List<String> obtenerCabecercostos() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "SELECT COLUMN_NAME "
                    + " FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='servicio'";

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String nombreservicio = rs.getString(1);

                listaTipos.add(nombreservicio);
            }
        }
        return listaTipos;
    }

}
