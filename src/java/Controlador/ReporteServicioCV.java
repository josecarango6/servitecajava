package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ReporteServicioCV", urlPatterns = {"/ReporteServicioCV"})
public class ReporteServicioCV extends HttpServlet {

 
    public static String Servicios[] = null;
    public static String tiporeporte = null;
    public static String documento = null;
    public static String placa = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        Servicios = request.getParameterValues("servicio");
     

        tiporeporte = request.getParameter("tipoReporte");
        documento = request.getParameter("documento");
        placa = request.getParameter("placa");

//         
        RequestDispatcher vista = request.getRequestDispatcher("/serviciosPrestadosCV.jsp");
        vista.forward(request, response);
//
//    
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
