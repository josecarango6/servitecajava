package modelo.DAO;

import Dbutil.Database;
import Entidades.Vehiculo;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jooss
 */
public class VehiculoDAO {

    private Connection conexion;

    public VehiculoDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public void guardar(Vehiculo u) {
        try {
            String sql = " INSERT INTO vehiculo (placa, fechaServicio, horaIngreso, FK_idTIPO_VEHICULO, FK_idCLIENTE) "
                    + " VALUES(?,?,?,?,?)";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setString(1, u.getPlaca());
            ps.setDate(2, u.getFechaServicio());
            ps.setString(3, u.getHoraIngreso());
            ps.setInt(4, u.getFK_idTIPO_VEHICULO());
            ps.setInt(5, u.getFK_idCLIENTE());
           
            //para cambios

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(VehiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenerIdtipoVeh(String tipoveh) throws SQLException {
        String sql = " SELECT idTIPO_VEHICULO FROM tipo_vehiculo "
                + " WHERE nombreTipoVehiculo = ?";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setString(1, tipoveh);

        ResultSet rs = ps1.executeQuery();

        int idtipoDoc = 0;
        if (rs.next()) {
            idtipoDoc = rs.getInt(1);
        }
        return idtipoDoc;
    }

    public int obtenerIdMasGrande() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "SELECT MAX(idVEHICULO) AS MAXID FROM  vehiculo";
        ResultSet rs = stmt.executeQuery(sql);

        int idMasGrande = 0;

        // cada vez que se ejecuta el método rs.next(), se avanza el cursor una fila 
        // Cuando se alcalza el fin del cursor, la funcion rs.next() retorna false
        if (rs.next()) {
            idMasGrande = rs.getInt("MAXID");
        }

        stmt.close();
        return idMasGrande;
    }

//    public int BuscarTurno() throws SQLException {
//        Statement stmt = conexion.createStatement();
//        String sql = "SELECT count(idVEHICULO) FROM  vehiculo";
//        ResultSet rs = stmt.executeQuery(sql);
//
//        int cont = 0;
//
//        if (rs.next()) {
//            cont = rs.getInt(1);
//        }
//
//        stmt.close();
//        return cont;
//    }
//
//    public Date ObtenerFecha(int id) throws SQLException {
//        String sql = " SELECT fechaServicio FROM vehiculo "
//                + " WHERE idVehiculo = ?";
//
//        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
//        PreparedStatement ps1 = conexion.prepareStatement(sql);
//
//        ps1.setInt(1, id);
//
//        ResultSet rs = ps1.executeQuery();
//
//        Date idtipoDoc = null;
//        if (rs.next()) {
//            idtipoDoc = rs.getDate(1);
//        }
//        return idtipoDoc;
//    }

}
