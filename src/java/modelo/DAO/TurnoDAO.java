package modelo.DAO;

import Dbutil.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TurnoDAO {

    private Connection conexion;

    public TurnoDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public List<String> obtenerTurnosactivos() throws SQLException {
        List<String> listaTipos = new LinkedList<>();

        try (Statement stmt = conexion.createStatement()) {
            String sql = "Select turno, vehiculo.placa, group_concat(' ',nombreServicio) as Servicios  from servicios_prestados inner join vehiculo on "
                    + " idVEHICULO=FK_idVEHICULO inner join servicio on idservicio=fk_idservicio where estadoTurno='0'  group by FK_idVEHICULO";
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                String turno = rs.getString(1);
                String placa=rs.getString(2);
                String servicios=rs.getString(3);

                listaTipos.add(turno);
                listaTipos.add(placa);
                listaTipos.add(servicios);
            }
        }
        return listaTipos;
    }
        public void modificarEstadoturno(int fk_idvehiculo) {
        try {
            String sql="update servicios_prestados set estadoTurno='1' where FK_idVEHICULO = ?";

            PreparedStatement ps = conexion.prepareStatement(sql);
            ps.setInt(1, fk_idvehiculo);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(ServicioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
