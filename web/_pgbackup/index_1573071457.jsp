<%-- 
    Document   : index
    Created on : 8/09/2019, 4:11:44 p. m.
    Author     : jooss
--%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!doctype html> 
<html lang="en"> 
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <meta name="description" content=""> 
        <meta name="author" content=""> 
        <title>Dashboard Template for Bootstrap</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
    </head>     
    <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item active text-white"> Inicio</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto px-4 col-lg-10"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2 text-danger">Inicio</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <% String tipoUsuarioindex = (String) session.getAttribute("tipoUsuario");
if (tipoUsuarioindex!="2") {
 
%> 
                        <div class="row"> 
                            <div class="text-white mb-3 col-md-3 mr-3 ml-3 card bg-primary" style="max-width: 18rem;" data-html="false"> 
                                <div class="card-header">Registro Vehiculo</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Registro de Vehiculos</h5> 
                                    <p class="card-text">Registro de los vehiculos que ingresan a la serviteca con sus determinados servicios</p> 
                                    <a href="registroVehiculo.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-3 mr-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Registro Cliente</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Registro de Clientes</h5> 
                                    <p class="card-text">Registro de los clientes que asisten a la serviteca para solicitar un servicio para su vehiculo</p> 
                                    <a href="registroCliente.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Servicios</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Lista de Servicios</h5> 
                                    <p class="card-text">Lista de los servicios prestados por la serviteca con sus determinados costos por tipo de vehiculo</p> 
                                    <a href="servicios.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-4 col-lg-3 mr-3 ml-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Liquidacion</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Liquidar Servicios</h5> 
                                    <p class="card-text">Permite liquidar todos los servicios asociados a un vehiculo</p> 
                                    <a href="liquidacion.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-4 col-lg-3 mr-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Descuentos</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Parametrizar Descuentos</h5> 
                                    <p class="card-text">Permite parametrizar los descuentos por cantidad de servicios prestados</p> 
                                    <a href="descuento.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-4 col-lg-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Turno</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Lista de Turnos sin Liquidar</h5> 
                                    <p class="card-text">Permite visualizar y liquidar todos los turnos que no han sido liquidados</p> 
                                    <a href="turno.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                        </div>                         
                        <%}else{%> 
                        <div class="row"> 
                            <div class="text-white mb-3 col-md-3 mr-3 ml-3 card bg-primary" style="max-width: 18rem;" data-html="false"> 
                                <div class="card-header">Registro Vehiculo</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Registro de Vehiculos</h5> 
                                    <p class="card-text">Registro de los vehiculos que ingresan a la serviteca con sus determinados servicios</p> 
                                    <a href="registroVehiculo.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-3 mr-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Registro Cliente</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Registro de Clientes</h5> 
                                    <p class="card-text">Registro de los clientes que asisten a la serviteca para solicitar un servicio para su vehiculo</p> 
                                    <a href="registroCliente.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-4 col-lg-3 mr-3 ml-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Liquidacion</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Liquidar Servicios</h5> 
                                    <p class="card-text">Permite liquidar todos los servicios asociados a un vehiculo</p> 
                                    <a href="liquidacion.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                            <div class="card text-white mb-3 col-md-4 col-lg-3 mr-3 ml-3 bg-primary" style="max-width: 18rem;"> 
                                <div class="card-header">Turno</div>                                 
                                <div class="card-body"> 
                                    <h5 class="card-title">Lista de Turnos sin Liquidar</h5> 
                                    <p class="card-text">Permite visualizar y liquidar todos los turnos que no han sido liquidados</p> 
                                    <a href="turno.jsp" class="btn btn-light">Abrir</a> 
                                </div>                                 
                            </div>                             
                        </div>                         
                    <%}%> 
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
    ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
      var ctx = document.getElementById("myChart");
      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
              datasets: [{
                  data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                  lineTension: 0,
                  backgroundColor: 'transparent',
                  borderColor: '#007bff',
                  borderWidth: 4,
                  pointBackgroundColor: '#007bff'
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: false
                      }
                  }]
              },
              legend: {
                  display: false,
              }
          }
      });
    </script>         
    </body>     
</html>
