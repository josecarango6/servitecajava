package Entidades;

import Entidades.Descuento;
import Entidades.Servicio;
import Entidades.Vehiculo;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-09-09T17:04:40")
@StaticMetamodel(ServiciosPrestados.class)
public class ServiciosPrestados_ { 

    public static volatile SingularAttribute<ServiciosPrestados, Integer> idSERVICIOSPRESTADOS;
    public static volatile SingularAttribute<ServiciosPrestados, Vehiculo> fKidVEHICULO;
    public static volatile SingularAttribute<ServiciosPrestados, Date> fecheServicio;
    public static volatile SingularAttribute<ServiciosPrestados, Servicio> fKidSERVICIO;
    public static volatile SingularAttribute<ServiciosPrestados, Descuento> fKidDESCUENTO;

}