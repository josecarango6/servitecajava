
package Entidades;

public class Servicio  {

    private String nombreServicio;
    private int costoAutomovil;
    private int costoCampero;
    private int costoCamion;
    private int costoBus;


    public Servicio() {
    }

    public Servicio(String nombreServicio, int costoAutomovil, int costoCampero, int costoCamion, int costoBus) {
        this.nombreServicio = nombreServicio;
        this.costoAutomovil = costoAutomovil;
        this.costoCampero = costoCampero;
        this.costoCamion = costoCamion;
        this.costoBus = costoBus;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public int getCostoAutomovil() {
        return costoAutomovil;
    }

    public void setCostoAutomovil(int costoAutomovil) {
        this.costoAutomovil = costoAutomovil;
    }

    public int getCostoCampero() {
        return costoCampero;
    }

    public void setCostoCampero(int costoCampero) {
        this.costoCampero = costoCampero;
    }

    public int getCostoCamion() {
        return costoCamion;
    }

    public void setCostoCamion(int costoCamion) {
        this.costoCamion = costoCamion;
    }

    public int getCostoBus() {
        return costoBus;
    }

    public void setCostoBus(int costoBus) {
        this.costoBus = costoBus;
    }

  

   


    
}
