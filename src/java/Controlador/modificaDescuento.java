
package Controlador;

import Entidades.Descuento;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAO.DescuentoDAO;


@WebServlet(name = "modificaDescuento", urlPatterns = {"/modificaDescuento"})
public class modificaDescuento extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
     
    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            int cantidad= Integer.parseInt(request.getParameter("cantidad"));
            int porcentaje= Integer.parseInt(request.getParameter("porcentaje"));
            DescuentoDAO desc= new DescuentoDAO();
            Descuento u = new Descuento(cantidad,porcentaje);
            desc.modificar(u);
            RequestDispatcher vista = request.getRequestDispatcher("/descuento.jsp");
            vista.forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(modificaDescuento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
