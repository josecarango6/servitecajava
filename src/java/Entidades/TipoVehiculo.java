
package Entidades;


public class TipoVehiculo{

    private int idTIPOVEHICULO;
    private String nombreTipoVehiculo;
    

    public TipoVehiculo() {
    }

    public TipoVehiculo(int idTIPOVEHICULO, String nombreTipoVehiculo) {
        this.idTIPOVEHICULO = idTIPOVEHICULO;
        this.nombreTipoVehiculo = nombreTipoVehiculo;
    }

    public int getIdTIPOVEHICULO() {
        return idTIPOVEHICULO;
    }

    public void setIdTIPOVEHICULO(int idTIPOVEHICULO) {
        this.idTIPOVEHICULO = idTIPOVEHICULO;
    }

    public String getNombreTipoVehiculo() {
        return nombreTipoVehiculo;
    }

    public void setNombreTipoVehiculo(String nombreTipoVehiculo) {
        this.nombreTipoVehiculo = nombreTipoVehiculo;
    }

   
    
}
