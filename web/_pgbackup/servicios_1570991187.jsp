<%-- 
    Document   : registroCliente
    Created on : 8/09/2019, 07:11:12 PM
    Author     : jooss
--%> 
<%@page import="static Controlador.servicioOpera.*"%> 
<%@page import="modelo.DAO.DescuentoDAO"%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="modelo.DAO.TipoDocumentoDAO"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="Entidades.TipoDocumento"%> 
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@page import=" static Controlador.consultaCliente.*"%> 
<!doctype html> 
<html> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
        %> 
    <head> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>         
        <title>Registro Cliente</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <link href="bootstrap/css/searchbox.css" rel="stylesheet" type="text/css"/> 
    </head>     
    <body id="cuerpito"> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Servicios</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <ul class="nav flex-column"> 
                            <li class="nav-item"> 
                                <a class="nav-link active" href="index.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"> 
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>                                         
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>                                         
                                    </svg>                                Inicio <span class="sr-only">(current)</span> </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="liquidacion.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"> 
                                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>                                         
                                        <polyline points="13 2 13 9 20 9"></polyline>                                         
                                    </svg> 
                                    Liquidación </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="registroVehiculo.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 11 11"> 
                                        <path d="M9 4l-.89-2.66A.5.5 0 0 0 7.64 1H3.36a.5.5 0 0 0-.47.34L2 4a1 1 0 0 0-1 1v3h1v1a1 1 0 1 0 2 0V8h3v1a1 1 0 1 0 2 0V8h1V5a1 1 0 0 0-1-1zM3 7a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm0-3l.62-2h3.76L8 4H3zm5 3a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" fill="#626262"></path>                                         
                                    </svg> 
                                    Registro Vehiculo </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="registroCliente.jsp" target=""> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"> 
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>                                         
                                        <circle cx="9" cy="7" r="4"></circle>                                         
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>                                         
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>                                         
                                    </svg> 
                                    Registro Cliente </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="#"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 20 20"> 
                                        <path d="M4 5H.78c-.37 0-.74.32-.69.84l1.56 9.99S3.5 8.47 3.86 6.7c.11-.53.61-.7.98-.7H10s-.7-2.08-.77-2.31C9.11 3.25 8.89 3 8.45 3H5.14c-.36 0-.7.23-.8.64C4.25 4.04 4 5 4 5zm4.88 0h-4s.42-1 .87-1h2.13c.48 0 1 1 1 1zM2.67 16.25c-.31.47-.76.75-1.26.75h15.73c.54 0 .92-.31 1.03-.83.44-2.19 1.68-8.44 1.68-8.44.07-.5-.3-.73-.62-.73H16V5.53c0-.16-.26-.53-.66-.53h-3.76c-.52 0-.87.58-.87.58L10 7H5.59c-.32 0-.63.19-.69.5 0 0-1.59 6.7-1.72 7.33-.07.37-.22.99-.51 1.42zM15.38 7H11s.58-1 1.13-1h2.29c.71 0 .96 1 .96 1z" fill="#626262"></path>                                         
                                    </svg> 
                                    Servicios </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="descuento.jsp"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 1024 1024"> 
                                        <path d="M855.7 210.8l-42.4-42.4a8.03 8.03 0 0 0-11.3 0L168.3 801.9a8.03 8.03 0 0 0 0 11.3l42.4 42.4c3.1 3.1 8.2 3.1 11.3 0L855.6 222c3.2-3 3.2-8.1.1-11.2zM304 448c79.4 0 144-64.6 144-144s-64.6-144-144-144-144 64.6-144 144 64.6 144 144 144zm0-216c39.7 0 72 32.3 72 72s-32.3 72-72 72-72-32.3-72-72 32.3-72 72-72zm416 344c-79.4 0-144 64.6-144 144s64.6 144 144 144 144-64.6 144-144-64.6-144-144-144zm0 216c-39.7 0-72-32.3-72-72s32.3-72 72-72 72 32.3 72 72-32.3 72-72 72z" fill="#626262"></path>                                         
                                    </svg> 
                                    Descuentos</a> 
                            </li>                             
                        </ul>                         
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted"> <span class="text-primary">Reportes</span> <a class="d-flex align-items-center text-muted" href="#"> </a> </h6> 
                        <ul class="nav flex-column mb-2"> 
                            <li class="nav-item"> 
                                <a class="nav-link" href="serviciosPrestados.jsp"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"> 
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                                        <polyline points="14 2 14 8 20 8"></polyline>                                         
                                        <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                                        <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                                        <polyline points="10 9 9 9 8 9"></polyline>                                         
                                    </svg> 
                                    Cantidad de Servicios Prestados y Tiempo de Atención </a> 
                            </li>                             
                            <li class="nav-item"> 
                                <a class="nav-link" href="serviciosPrestadosCV.jsp"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"> 
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>                                         
                                        <polyline points="14 2 14 8 20 8"></polyline>                                         
                                        <line x1="16" y1="13" x2="8" y2="13"></line>                                         
                                        <line x1="16" y1="17" x2="8" y2="17"></line>                                         
                                        <polyline points="10 9 9 9 8 9"></polyline>                                         
                                    </svg> 
                                    Servicios Prestados por Cliente o Automotor </a> 
                            </li>                             
                            <li class="nav-item"> 
</li>                             
                            <li class="nav-item"> 
</li>                             
                        </ul>                         
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mb-5"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Servicios</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <!--cuadro de busqueda-->                                 
                                <div class='search-container'> 
</div>                                 
                                <!--cuadro de busqueda-->                                 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <script>
                        var dato = [];
                        var dato2 = '';
                        
                        $(document).ready(function () {
                            
                            var limite = getBody($('table.Tablefu'));
                            
                            
                            $('#table-list tr').on('click', function () {
                                
                             //quitarformato();
                                for (var i = 0; i < limite; i++) {
                                    $(this).find("td:eq(" + i + ")").attr("contenteditable", "true");
                                    $(this).find("td:eq(" + i + ")").css('background-color', 'gray');
                                    
                                    
                                    if (i === 0) {
                                        document.getElementById("seleccionado").value = $(this).find("td:eq(" + i + ")").html();
                                        dato2 = document.getElementById("seleccionado").value;
                                        
                                    }
                                    
                                }
                            });
                            $('#table-list tr').on('mouseout', function () {
                                
                                for (var i = 0; i < limite; i++) {
                                    $(this).find("td:eq(" + i + ")").attr("contenteditable", "false");
                                    $(this).find("td:eq(" + i + ")").css('background-color', 'white');
                                    
                                    
                                    
                                }
                                
                            });
                            $('#table-list tr').on('keyup', function () {
                                for (var i = 0; i < limite; i++) {
                                    dato[i] = $(this).find("td:eq(" + i + ")").html();
                                }
                                
                            });
                            $('#table-list').on('mouseout', function () {
                                
                                
                            });
                        });
                        
                        
                        function getBody(element) {
                            
                            var originalTable = element.clone();
                            var tds = $(originalTable).children('tbody').children('tr').length;
                            return tds;
                        }
                        
                        
                        
                        
                    </script>                     
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left bg-white pr-5 pb-5 pl-5 col-md-12"> 
                            <h4 class="mb-3 mt-auto">Costo de los Servicios</h4> 
                            <%  ServicioDAO servic = new ServicioDAO();
                            
                                servic.modificar(concatenado, nombreservicio);
                                concatenado = "";
                                nombreservicio = "";
                            %> 
                            <!--<form class="needs-validation" novalidate="" method="post" action="registroCliente"> -->                             
                            <div class="row"> 
                                <div class="table-content col-md-12 "> 
                                    <table class="Tablefu table table-borded " id="table-list"> 
                                        <thead class="table-dark"> 
                                            <tr> 
                                                <%-- <%DescuentoDAO ddao = new DescuentoDAO();
                                                    List<String> lista1 = new LinkedList();
                                                    lista1 = ddao.obtenerCabecercostos();
                                                    for (int i = 1; i < lista1.size(); i++) {%> 
                                                    <th><%out.print(lista1.get(i));%></th> 
                                                <%}%> --%>
                                                <th>nombreServicio</th> 
                                                <th>costoAutomovil</th> 
                                                <th>costoCampero</th> 
                                                <th>costoCamion</th> 
                                                <th>costoBus</th> 
                                            </tr>                                             
                                        </thead>                                         
                                            <tbody> 
                                            <%
                                                ServicioDAO servi = new ServicioDAO();
                                                List<String> listaTipos = new LinkedList();
                                                listaTipos = servi.obtenerTodosLosServiciosconcosto();
                                                for (int i = 0; i < listaTipos.size(); i++) {%> 
                                            <tr> 
                                                    <td scope="row"><%out.print(listaTipos.get(i));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 1));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 2));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 3));%></td> 
                                                    <td name="costos"><%out.print(listaTipos.get(i + 4));%></td> 
                                                    <%i = i + 4;%> 
                                                <%}%> 
                                                </tr>                                                 
                                        </tbody>                                         
                                    </table>                                     
                                    <input style="display: none" id="seleccionado" name="borra" readonly=""> 
                                    <button onclick="window.location.href = 'http://localhost:8080/Serviteca/servicioOpera?dato=' + dato + '&dato2=' + dato2 + '&op=modifica'" class="btn btn-info" id="add"> 
                                        <span class="glyphicon glyphicon-plus-sign"></span>Modificar
                                    </button>                                     
                                    <button onclick="window.location.href = 'http://localhost:8080/Serviteca/servicioOperaborra?dato2=' + dato2 + '&op=borra'" class="btn btn-info" id="add"> 
                                        <span class="glyphicon glyphicon-plus-sign"></span>Eliminar
                                    </button>                                     
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                        Agregar
</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Agregar un Nuevo Servicio</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    ..........................................................
                                                    <div class="row col-lg-12">
                                                        <div class="col-md-6 mb-3 col-lg-5">
                                                            <label for="firstName">Nombre del Servicio</label>
                                                            <input type="text" class="form-control" id="nombreServicio" placeholder="" value="" required>
                                                            <div class="invalid-feedback">
                                                                Valid first name is required.
</div>
                                                        </div>
                                                        <div class="col-md-6 mb-3 col-lg-7">
                                                            <div class="invalid-feedback">
                                                                Valid first name is required.
</div>
                                                        </div>
                                                        <div class="col-md-6 mb-3 col-lg-6">
                                                            <label for="lastName">Costo Automovil</label>
                                                            <input type="text" class="form-control" id="costoAutomovil" placeholder="" value="" required>
                                                            <div class="invalid-feedback">
                                                                Valid last name is required.
</div>
                                                        </div>
                                                        <div class="col-md-6 mb-3 col-lg-6">
                                                            <label for="lastName">Costo Campero</label>
                                                            <input type="text" class="form-control" id="costoCampero" placeholder="" value="" required>
                                                            <div class="invalid-feedback">
                                                                Valid last name is required.
</div>
                                                        </div>
                                                        <div class="col-md-6 mb-3 col-lg-6">
                                                            <label for="lastName">Costo Camion</label>
                                                            <input type="text" class="form-control" id="costoCamion" placeholder="" value="" required>
                                                            <div class="invalid-feedback">
                                                                Valid last name is required.
</div>
                                                        </div>
                                                        <div class="col-md-6 mb-3 col-lg-6">
                                                            <label for="lastName">Costo Bus</label>
                                                            <input type="text" class="form-control" id="costoBus" placeholder="" value="" required>
                                                            <div class="invalid-feedback">
                                                                Valid last name is required.
</div>
                                                        </div>
                                                    </div>
                                                    ..........................................................
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" class="btn btn-primary">Agregar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                     
                                    <!-- <button onclick="quitarformato()" class="btn btn-info" id="add">
                                        <span class="glyphicon glyphicon-plus-sign"></span>Quitar formato
                                    </button>  -->                                     
                                </div>                                 
                                <script>
                                    
                                    document.addEventListener("DOMContentLoaded", function (event) {
                                        
                                        //darformato();
                                        
                                        
                                        
                                    });
                                    
//sin formato decimal
                                    function darformato() {//le da formato decimal a los precios
                                        
                                        var valores = document.getElementsByName("costos");
                                        for (var i = 0; i < valores.length; i++) {
                                            
                                            if (valores[i].innerHTML !== "") {
                                                valores[i].innerHTML = valores[i].innerHTML
                                                        .replace(/\D/g, "")
                                                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                                            }
                                        }
                                    }
                                    function quitarformato() {
                                        var valores = document.getElementsByName("costos");
                                        for (var i = 0; i < valores.length; i++) {
                                            
                                            var a = valores[i].innerHTML;
                                            if (a.split(".")[0]!==undefined && a.split(".")[1]!==undefined) {
                                                valores[i].innerHTML = a.split(".")[0] + a.split(".")[1];
                                            }
                                            
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    
                                </script>                                 
                            </div>                             
                            <hr class="mb-4"> 
                            <!-- </form>   -->                             
                        </div>                         
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
    ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
                                    var ctx = document.getElementById("myChart");
                                    var myChart = new Chart(ctx, {
                                        type: 'line',
                                        data: {
                                            labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                            datasets: [{
                                                    data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                    lineTension: 0,
                                                    backgroundColor: 'transparent',
                                                    borderColor: '#007bff',
                                                    borderWidth: 4,
                                                    pointBackgroundColor: '#007bff'
                                                }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                        ticks: {
                                                            beginAtZero: false
                                                        }
                                                    }]
                                            },
                                            legend: {
                                                display: false,
                                            }
                                        }
                                    }
                                    );
        </script>         
    </body>     
</html>
