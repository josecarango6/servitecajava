package modelo.DAO;

import Dbutil.Database;
import Entidades.Liquidacion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LiquidacionDAO {

    private Connection conexion;

    public LiquidacionDAO() throws Exception {
        conexion = Database.getConexion();
    }

    public int cantidadServicio() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "SELECT cantidadServicios FROM  descuento";
        ResultSet rs = stmt.executeQuery(sql);

        int cantidad = 0;

        if (rs.next()) {
            cantidad = rs.getInt(1);
        }

        stmt.close();
        return cantidad;
    }

    public int PorcentajeDesc() throws SQLException {
        Statement stmt = conexion.createStatement();
        String sql = "SELECT porcentajeDescuento FROM  descuento";
        ResultSet rs = stmt.executeQuery(sql);

        int porc = 0;

        // cada vez que se ejecuta el método rs.next(), se avanza el cursor una fila 
        // Cuando se alcalza el fin del cursor, la funcion rs.next() retorna false
        if (rs.next()) {
            porc = rs.getInt(1);
        }

        stmt.close();
        return porc;
    }

    public int consultaIdVehiculo(int turno, String placa) throws SQLException {

        String sql = "select FK_idVEHICULO from servicios_prestados  INNER join "
                + " vehiculo on FK_idVEHICULO=idVEHICULO  where turno = ? "
                + " and  placa = ? and estadoTurno='0' group by FK_idVEHICULO limit 1";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, turno);

        ps1.setString(2, placa);

        ResultSet rs = ps1.executeQuery();
        int idveh = 0;
        while (rs.next()) {

            idveh = rs.getInt(1);

        }

        ps1.close();
        return idveh;
    }

    public void guardar(Liquidacion u) {

        try {
            String sql = " INSERT INTO Liquidacion ( FK_idDESCUENTO, fechaIngreso,horaIngreso, costoServicios, totalLiquidacion, fechaSalida, horaSalida,tiempoServicio,diasServicio, FK_idVEHICULO, FK_idESTADO_LIQUIDACION) "
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = conexion.prepareStatement(sql);

            ps.setInt(1, u.getFK_idDESCUENTO());
            ps.setDate(2, u.getFechaIngreso());
            ps.setString(3, u.getHoraIngreso());
            ps.setInt(4, u.getCostoServicios());
            ps.setInt(5, u.getTotalLiquidacion());
            ps.setDate(6, u.getFechaSalida());
            ps.setString(7, u.getHoraSalida());
            ps.setString(8, u.getTiempoServicio());
            ps.setInt(9, u.getDiasServicio());
            ps.setInt(10, u.getFK_idVEHICULO());
            ps.setInt(11, u.getFK_idESTADO_LIQUIDACION());

            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(LiquidacionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Date consultarfecha(int idvehiculo) throws SQLException {

        String sql = "SELECT fechaServicio from vehiculo inner join servicios_prestados on "
                + " fk_idvehiculo=idvehiculo where FK_idVEHICULO = ? group by turno limit 1 ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, idvehiculo);

        ResultSet rs = ps1.executeQuery();
        Date fecha = new Date(0);
        while (rs.next()) {

            fecha = rs.getDate(1);

        }

        ps1.close();
        return fecha;
    }

    public String consultarhora(int idvehiculo) throws SQLException {

        String sql = "SELECT horaIngreso from vehiculo inner join servicios_prestados on "
                + " fk_idvehiculo=idvehiculo where FK_idVEHICULO = ? group by turno limit 1 ";

        // La clase PreparedStatement se usa cuando la instrucción de SQL tiene parámetros
        PreparedStatement ps1 = conexion.prepareStatement(sql);

        ps1.setInt(1, idvehiculo);

        ResultSet rs = ps1.executeQuery();
        String fecha = "";
        while (rs.next()) {

            fecha = rs.getString(1);

        }

        ps1.close();
        return fecha;
    }
    

}
