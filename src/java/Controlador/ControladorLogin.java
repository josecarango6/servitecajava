package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.DAO.UsuarioDAO;

@WebServlet(name = "ControladorLogin", urlPatterns = {"/ControladorLogin"})
public class ControladorLogin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String registreseOcrear = (String) request.getParameter("Salir");

        if (registreseOcrear.equals("logout")) {
            // Cierra la sesión del usuario
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect("index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
            List<String> listadmin = new LinkedList<>();
            List<String> listaopera = new LinkedList<>();
            UsuarioDAO us = new UsuarioDAO();
            listadmin=us.Tipousuario(1);
            listaopera=us.Tipousuario(2);
           

            ////
       

            String usuario = request.getParameter("usuario");
            String clave = request.getParameter("clave");
//primera comparacion con datos de la base de datos
            if ((usuario.equalsIgnoreCase(listadmin.get(0)) && clave.equals(listadmin.get(1))) && listadmin.get(2).equals("1")) {
                //Crea una session para el administrador de la aplicación
                HttpSession session = request.getSession();
                session.setAttribute("nombreUsuario", "admin");//se guarda en session el nombre del usuario
                session.setAttribute("tipoUsuario", "1");//se guarda en session el tipo de usuario
                response.sendRedirect("index.jsp");
            } else if ((usuario.equalsIgnoreCase(listaopera.get(0)) && clave.equals(listaopera.get(1))) && listaopera.get(2).equals("2")) {
                // El usuario ya está registrado
                HttpSession session = request.getSession();
                session.setAttribute("nombreUsuario", usuario);
                session.setAttribute("tipoUsuario", "2");
                response.sendRedirect("index.jsp");
            } else {
                //request.setAttribute("usuarioInvalido", "usuarioInvalido");
//dispara un mensaje
                RequestDispatcher vista = request.getRequestDispatcher("/login.jsp");
                vista.forward(request, response);
            }
        } catch (Exception ex) {
            Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
