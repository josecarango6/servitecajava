<%-- 
    Document   : registroCliente
    Created on : 8/09/2019, 07:11:12 PM
    Author     : jooss
--%> 
<%@page import="modelo.DAO.ServicioDAO"%> 
<%@page import="modelo.DAO.TipoDocumentoDAO"%> 
<%@page import="java.util.LinkedList"%> 
<%@page import="Entidades.TipoDocumento"%> 
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@page import=" static Controlador.consultaCliente.*"%> 
<!doctype html> 
<html style="background: linear-gradient(90deg, #4b6cb7, #182848);"> 
    <%
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setHeader("Expires", "0"); // Proxies.
%> 
    <head> 
        <title>Registro Cliente</title>         
        <!-- Bootstrap core CSS -->         
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> 
        <!-- Custom styles for this template -->         
        <link href="dashboard.css" rel="stylesheet"> 
        <link href="bootstrap/css/searchbox.css" rel="stylesheet" type="text/css"/> 
    </head>     
    <body> 
        <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow"> 
            <a class="navbar-brand col-sm-3 mr-0 col-md-2" href="#">Service Station System</a> 
            <ol class="breadcrumb m-auto p-auto bg-dark"> 
                <li class="breadcrumb-item"> 
                    <a href="index.jsp">Inicio</a> 
                </li>                 
                <li class="breadcrumb-item active text-white">Registro Cliente</li>                 
            </ol>             
            <ul class="navbar-nav px-3"> 
                <li class="nav-item text-nowrap"> 
                    <%@include file="loginLogout.jsp" %> 
                </li>                 
            </ul>             
        </nav>         
        <div class="container-fluid"> 
            <div class="row"> 
                <nav class="col-md-2 d-none d-md-block bg-light sidebar"> 
                    <div class="sidebar-sticky"> 
                        <%@include file="menu.jsp" %> 
                    </div>                     
                </nav>                 
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
                    <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"> 
                        <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>                             
                        </div>                         
                        <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"> 
                            <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>                             
                        </div>                         
                    </div>                     
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> 
                        <h1 class="h2">Registro del Cliente</h1> 
                        <div class="btn-toolbar mb-2 mb-md-0"> 
                            <div class="btn-group mr-2"> 
                                <!--cuadro de busqueda-->                                 
                                <div class='search-container'> 
                                    <script>
                                        document.addEventListener("DOMContentLoaded", function (event) {
                                        document.getElementById("busqueda").setIdAttribute("autocomplete", "off");
                                        if (document.readyState !== "complete") {
                                        document.getElementById("busqueda").setIdAttribute("autocomplete", "on");
                                        }

                                        });
                                        function guarda() {
                                        var val = document.getElementById('busqueda').value;
                                        return val;
                                        }
                                        function stopSubmit(e) {
                                        var inDay = document.getElementById('busqueda').value;
                                        if (inDay == "") {
                                        return false;
                                        }
                                        }
                                    </script>                                     
                                    <a id="accionar" style="display: none" href="consultaCliente" onclick="stopSubmit();
                                        window.location.reload(true);
                                        window.open(this.href += '?cedula=' + guarda(), 'newwindow', 'width=300, height=100, top=' + (screen.height - 250) / 2 + ',left=' + (screen.width - 300) / 2 + ',location=no');
                                        return false;" target="_blank" class='button'> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveaspectratio="xMidYMid meet" viewbox="0 0 24 24"> 
                                            <path d="M15.5 12a4.5 4.5 0 0 1 3.807 6.9l3.084 3.084-1.407 1.407-3.108-3.069A4.5 4.5 0 1 1 15.5 12zm0 2a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM10 4a4 4 0 0 1 3.18 6.426 6.505 6.505 0 0 0-2.268 1.47L10 12a4 4 0 0 1 0-8zM2 20v-2c0-2.124 3.312-3.862 7.495-3.992A6.48 6.48 0 0 0 9 16.5a6.47 6.47 0 0 0 1.022 3.5H2z" fill="#626262"/> 
                                        </svg> </a> 
                                </div>                                 
                                <!--cuadro de busqueda-->                                 
                                <button class="btn btn-sm btn-outline-secondary">Share</button>                                 
                                <button class="btn btn-sm btn-outline-secondary">Export</button>                                 
                            </div>                             
                            <button class="btn btn-sm btn-outline-secondary dropdown-toggle"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"> 
                                    <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>                                     
                                    <line x1="16" y1="2" x2="16" y2="6"></line>                                     
                                    <line x1="8" y1="2" x2="8" y2="6"></line>                                     
                                    <line x1="3" y1="10" x2="21" y2="10"></line>                                     
                                </svg>                                 
                                This week
                            </button>                             
                        </div>                         
                    </div>                     
                    <div class="row text-left"> 
                        <div class="order-md-1 text-left pr-5 pl-5 col-md-12 mb-5 pb-5"> 
                            <div class="row"> 
                                <h4 class="mb-3 mt-auto">Datos del Cliente</h4> 
                            </div>
                            <form class="container" id="needs-validation" novalidate method="post" action="registroCliente"> 
                                <div class="row"> 
                                    <div class="col-md-6 mb-3 col-lg-4">Tipo de Documento
                                        <br> 
                                        <div class="dropdown"> 
                                            <select class="custom-select d-block w-100" id="tipoDocumento" name="tipoDocumento" required> 
                                                <option value="" disabled selected="disabled selected">seleccione una opcion</option>                                                 
                                                <%                                                    List<String> lstDoc = new LinkedList();
                                                    TipoDocumentoDAO tipo = new TipoDocumentoDAO();
                                                    lstDoc = tipo.obtenerTodosLosTipos();//LLenandose :)
                                                    for (int i = 0; i < lstDoc.size(); i++) {
                                                        out.print("<option>" + lstDoc.get(i) + "</option>");
                                                    }
                                                %> 
                                            </select>                                             
                                            <div class="invalid-feedback"> 
                                                Este campo es requerido
</div>                                             
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;"> 
                                                <a class="dropdown-item" href="#">Action</a> 
                                                <a class="dropdown-item" href="#">Another action</a> 
                                                <a class="dropdown-item" href="#">Something else here</a> 
                                            </div>                                             
                                        </div>                                         
                                    </div>                                     
                                    <div class="mb-3 col-md-3 col-lg-4">Numero Documento
                                        <br> 
                                        <input autocomplete="off" type="text" id="myInput" oninput="myFunction()" class="form-control" id="busqueda" name="numeroDocumento" placeholder="" required=""> 
                                        <p id="demo"></p> 
                                        <script>
                                          
                                            function myFunction() {
                                            var x = document.getElementById("myInput").value;
                                            var datadoc = [];
                                        <% List<String> listaDocumentos = new LinkedList();
                                            ServicioDAO documentos = new ServicioDAO();
                                            listaDocumentos = documentos.obtenerDocumentos();

                                            for (int i = 0; i < listaDocumentos.size(); i++) {%>
                                            datadoc[<%out.print(i);%>] = "<%out.print(listaDocumentos.get(i));%>";
                                        <%}%>
                                            for (var j = 0; j < datadoc.length; j++) {
                                            if (x === datadoc[j]) {
                                            document.getElementById("demo").innerText = "Ya existe la cedula: " + x;
                                            myFunction();
                                            }
                                               if (x !== datadoc[j]) {
                                            document.getElementById("demo").innerText = "No existe ";
                                            myFunction();
                                            }
                                         

                                            }



                                            }
                                            
                                        </script>                                         
                                        <script>


                                            document.addEventListener("DOMContentLoaded", function (event) {



                                            if (document.readyState !== "complete") {
                                            <%valor = null;%>
                                            }

                                            });
                                        </script>                                         
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-4 mb-3">Primer Nombre
                                        <br> 
                                        <input type="text" class="form-control" id="validationCustom03" name="primerNombre" placeholder="" required=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4">Segundo Nombre
                                        <br> 
                                        <input type="text" class="form-control" id="segundoNombre" name="segundoNombre" placeholder="" value=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4">Primer Apellido
                                        <br> 
                                        <input type="text" class="form-control" id="primerApellido" name="primerApellido" placeholder="" value="" required=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4">Segundo Apellido
                                        <br> 
                                        <input type="text" class="form-control" id="segundoApellido" name="segundoApellido" placeholder="" value="" required=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                </div>                                 
                                <div class="row"> 
                                    <div class="col-md-6 mb-3 col-lg-4">Email
                                        <br> 
                                        <input type="email" class="form-control" id="email" name="correo" placeholder="ejemplo@ejemplo.com" required=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4">Teléfono
                                        <br> 
                                        <input type="number" class="form-control" id="telefono" name="telefono" placeholder="" value="" required=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                    <div class="col-md-6 mb-3 col-lg-4">Dirección
                                        <br> 
                                        <input type="text" class="form-control" id="address" name="direccion" placeholder="Carrera 1 # 1-2" value="" required=""> 
                                        <div class="invalid-feedback"> 
                                            Este campo es requerido
</div>                                         
                                    </div>                                     
                                </div>                                 
                                <hr class="mb-4"> 
                                <div class="row"> 
                                    <button class="btn btn-primary btn-lg btn-block m-auto col-md-4" type="submit">Registrar Cliente</button>                                     
                                </div>                                 
                            </form>                             
                            <script src="assets/js/jquery.min.js">
                            <script type="bootstrap/js/bootstrap.min.js" </script>                             
                            <script>
                                // Example starter JavaScript for disabling form submissions if there are invalid fields
                                (function() {
                                                    'use strict';
                                            window.addEventListener('load', function() {
                                            var form = document.getElementById('needs-validation');
                                            form.addEventListener('submit', function(event) {
                                            if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                                        }, false);
                                        }, false);
                                        })();
                                        </script>                             
                        </div>
                    </div>                     
                </main>                 
            </div>             
        </div>         
        <!-- Bootstrap core JavaScript
        ================================================== -->         
        <!-- Placed at the end of the document so the pages load faster -->         
        <script src="assets/js/jquery.min.js"></script>         
        <script src="assets/js/popper.js"></script>         
        <script src="bootstrap/js/bootstrap.min.js"></script>         
        <!-- Icons -->         
        <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>         
        <script>feather.replace()</script>         
        <!-- Graphs -->         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>         
        <script>
                                var ctx = document.getElementById("myChart");
                                var myChart = new Chart(ctx, {
                                            type: 'line',
                                                    data: {
                                                    labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                                            datasets: [{
                                                            data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                                                                    lineTension: 0,
                                                                    backgroundColor: 'transparent',
                                                                    borderColor: '#007bff',
                                                                    borderWidth: 4,
                                                                    pointBackgroundColor: '#007bff'
                                }]
                                },
                                options: {
                                                                    scales: {
                                                                    yAxes: [{
                                                                    ticks: {
                                                                    beginAtZero: false
                                }
                                }]
                                },
                                legend: {
                                                                            display: false,
                                }
                                }
                                }
                                );
                                </script>         
    </body>     
</html>
